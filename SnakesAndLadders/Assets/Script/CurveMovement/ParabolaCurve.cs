﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParabolaCurve
{
  public Vector3 SampleParabola(Vector3 start,Vector3 end,float height,float t)
    {
        Vector3 travelDirection = end - start;
        Vector3 result = start + travelDirection * t;
        result.y += Mathf.Sin(Mathf.PI * t) * height;

        return result;
    }
}
