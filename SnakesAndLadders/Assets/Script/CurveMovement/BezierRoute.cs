﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BezierRoute : MonoBehaviour
{
    public Transform A;
    public Transform B;
    public Transform p0;
    public Transform p1;

    private int lineNode = 20;
    private BezierCurve curve = new BezierCurve();

    public Vector3 getCurvePosition(float time)
    {
        return curve.CubicCurve(A.position, p0.position, p1.position, B.position, time);
    }

    private void OnDrawGizmos()
    {
        if (A == null || B == null)
            return;
        Gizmos.color = Color.green;
        Gizmos.DrawLine(A.position, B.position);

        if (p0 == null || p1 == null)
            return;
        Gizmos.color = Color.grey;
        Gizmos.DrawLine(A.position, p0.position);
        Gizmos.DrawLine(B.position, p1.position);
        Gizmos.color = Color.white;
        Vector3 lineStart = curve.CubicCurve(A.position, p0.position, p1.position, B.position, 0);
        for (int i = 0; i <= lineNode; i++)
        {
            float cal = i / (float)lineNode;
            Vector3 lineEnd = curve.CubicCurve(A.position, p0.position, p1.position, B.position, cal);
            Gizmos.DrawLine(lineStart, lineEnd);
            lineStart = lineEnd;
        }
    }
}
