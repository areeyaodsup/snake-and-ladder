﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class SoundManager : MonoBehaviour
{
    public const float MUTE_VOLUME = -80;
    static public SoundManager Instance;
    //{
    //    get
    //    {
    //        Debug.Log("1");
    //        if (singleton_instance == null)
    //        {
    //            Debug.Log("1.1");
    //            singleton_instance = FindObjectOfType<SoundManager>();
    //            if (singleton_instance != null)
    //                return null;
    //            Debug.Log("1.2");
    //            GameObject container = new GameObject("SoundManagerGet");
    //            singleton_instance = container.AddComponent<SoundManager>();
    //        }
    //        return singleton_instance;
    //    }
    //}
    static protected SoundManager singleton_instance = null;
    [SerializeField]
    AudioMixer mixer;
    public AudioMixer Mixer
    {
        get { return mixer; }
    }
    public float MusicVolume
    {
        get
        {
            return musicVolume;
        }
        set
        {
            musicVolume = value;
            SoundManager.Instance.Mixer.SetFloat("MusicVolume", musicVolume);
        }
    }
    float musicVolume;
    public float MusicVolumeDefault
    {
        get
        {
            return musicVolumeDefault;
        }
    }
    float musicVolumeDefault;
    public float MasterSFXVolume
    {
        get
        {
            return masterSFXVolume;
        }
        set
        {
            masterSFXVolume = value;
            SoundManager.Instance.Mixer.SetFloat("MasterSFXVolume", masterSFXVolume);
        }
    }
    float masterSFXVolume;
    public float MasterSFXVolumeDefault
    {
        get
        {
            return masterSFXVolumeDefault;
        }
    }
    float masterSFXVolumeDefault;
    AudioSource audioSourceBGM;
    public AudioSource BGMSource
    {
        get
        {
            return audioSourceBGM;
        }
    }
    public float fadeDuration = 0.75f;
    IEnumerator coroutine;
    void Awake()
    {
        //Debug.unityLogger.Log(name, "<b><color=yellow>2</color></b>");
        //Debug.unityLogger.Log(name, singleton_instance);
        if (singleton_instance == null)
        {
            Instance = this;
            singleton_instance = this;
            DontDestroyOnLoad(this.gameObject);

            //Important, execute these statements only 1st time singleton instance creation
            float musicVolume;
            if (mixer.GetFloat("MusicVolume", out musicVolume))
                singleton_instance.musicVolumeDefault = musicVolume;
            float masterSFXVolume;
            if (mixer.GetFloat("MasterSFXVolume", out masterSFXVolume))
                singleton_instance.masterSFXVolumeDefault = masterSFXVolume;

            audioSourceBGM = this.GetComponent<AudioSource>();
            audioSourceBGM.volume = 1.0f;
        }
        else if(singleton_instance!=null)
        {
            if (this != singleton_instance)
            {
                //Debug.unityLogger.Log(name, "<b><color=yellow>3</color></b>");
                //Debug.Log(name + " destroyed by singleton " + singleton_instance);
                Destroy(this.gameObject);
            }
        }
       
    }
    // Use this for initialization
    void Start()
    {
        //audioSourceBGM = this.GetComponent<AudioSource>();
        //audioSourceBGM.volume = 1.0f;
    }
    // Update is called once per frame
    public void AudioSoundVolumeUpdate() 
    {
        float musicVolume;
        if (mixer.GetFloat("MusicVolume", out musicVolume))
            musicVolumeDefault = musicVolume;
        float masterSFXVolume;
        if (mixer.GetFloat("MasterSFXVolume", out masterSFXVolume))
           masterSFXVolumeDefault = masterSFXVolume;
    }
    public void PlayBGM(AudioClip bgmClip)
    {
        Debug.unityLogger.Log("Play new BGM");
        if (audioSourceBGM.isPlaying)
            audioSourceBGM.Stop();
        audioSourceBGM.clip=bgmClip;
        audioSourceBGM.Play();
    }
    public void PlayBGMWithFadeIn(AudioClip bgmClip)
    {
        //Debug.Assert(audioSourceBGM, "missing audio source");
        //Debug.Assert(!audioSourceBGM, "have audio source");
        Debug.unityLogger.Log("Play new BGM");
        if (audioSourceBGM.isPlaying) audioSourceBGM.Stop();
        audioSourceBGM.clip = bgmClip;
        audioSourceBGM.Play();
        VolumeFadeIn();
    }
    public void VolumeFadeIn()
    {
        if (coroutine == null)
        {
            coroutine = FadeSoundVolume(0,1);
            StartCoroutine(coroutine);
            //Debug.Log("fade in");
        }
    }
    public void VolumeFadeOut() 
    {
        if(coroutine == null)
        {
            coroutine = FadeSoundVolume(1, 0);
            StartCoroutine(coroutine);
            //Debug.Log("fade out");
        }    
    }
    IEnumerator FadeSoundVolume(float startVolume, float endVolume)
    {
        float progress = 0.0f;
        while (progress <= 1)
        {
            progress += Time.unscaledDeltaTime * fadeDuration;
            audioSourceBGM.volume = Mathf.Lerp(startVolume, endVolume, progress);
            yield return null;
        }
        //Debug.Log("fade complete");
        coroutine = null;
    }
}
