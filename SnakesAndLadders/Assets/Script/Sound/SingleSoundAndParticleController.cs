﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class SingleSoundAndParticleController : MonoBehaviour
{
    [SerializeField] AudioSource audioSource;
    public AudioClip sound;
    [Header("Particle")]
    [SerializeField] Transform particleSpawn;
    public GameObject particlePrefab;
    public float particleDisableDeley = 1;
    void Start()
    {
        if (audioSource == null)
            audioSource = this.GetComponent<AudioSource>();
        audioSource.outputAudioMixerGroup = SoundManager.Instance.Mixer.FindMatchingGroups("SFX")[1];
    }

    void PlaySound()
    {
        audioSource.PlayOneShot(sound);
    }
    void StartParticle()
    {
        GameObject particle = Instantiate(particlePrefab, particleSpawn);
        particle.transform.rotation = Quaternion.Euler(particleSpawn.eulerAngles);
        Destroy(particle, particleDisableDeley);
    }
}
