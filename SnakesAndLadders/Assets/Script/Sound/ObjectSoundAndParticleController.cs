﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class ObjectSoundAndParticleController : MonoBehaviour
{
    [SerializeField]AudioSource audioSource;
    public AudioClip firstEventSound;
    public AudioClip secondEventSound;
    [Header("Particle")]
    public GameObject particlePrefab;
    public float particleDisableDeley = 1;
    // Start is called before the first frame update
    void Start()
    {
        audioSource = this.GetComponent<AudioSource>();
        audioSource.outputAudioMixerGroup = SoundManager.Instance.Mixer.FindMatchingGroups("SFX")[1];
    }

    void PlayFirstSound()
    {
        audioSource.PlayOneShot(firstEventSound);
    }
    void PlaySecondSound()
    {
        if (audioSource.isPlaying) audioSource.Stop();
        audioSource.PlayOneShot(secondEventSound);
    }
    void StartParticle()
    {
        StartCoroutine("PlayParticle");
    }

    IEnumerator PlayParticle()
    {
        particlePrefab.SetActive(true);
        yield return new WaitForSeconds(particleDisableDeley);
        particlePrefab.SetActive(false);
    }
}
