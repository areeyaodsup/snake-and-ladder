﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileManager : MonoBehaviour
{
    public Tile startTile { get { return tiles[0]; } }
    public Tile fininshTile {get{ return tiles[tiles.Count - 1]; } }

    [SerializeField]protected GameObject tileGameObject; 
    public List<Tile> tiles;

   void Awake()
    {
        if (tiles.Count > 0)
            tiles.Clear();

        if (tileGameObject == null)
            return;

        foreach (Transform child in tileGameObject.transform)
        {
            Tile c = child.GetComponent<Tile>();
            tiles.Add(c);
        }

        string name = "";
        foreach (Tile t in tiles)
        {
            t.index = tiles.IndexOf(t);

            if (t.index == 0)
                name = "Start Tile " + t.index.ToString();
            else if (t.index == tiles.Count - 1)
                name = "Finish Tile " + t.index.ToString();
            else if (t.myEvent)
                name = t.myEvent.eventName + " " + t.index.ToString();
            else
                name = "tile " + t.index.ToString();

            t.name = name;
        }
    }

    public Tile FindCheckPoint(int tileIndex)
    {
        for (int i = tileIndex-1; i >0; i--)
        {
            if (tiles[i].Ischeckpoint)
                return tiles[i];
        }

        return startTile;
    }
}
