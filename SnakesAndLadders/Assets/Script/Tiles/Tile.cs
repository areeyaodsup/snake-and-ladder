﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Tile : MonoBehaviour
{
    public int index { get { return tileIndex; } set { tileIndex = value; } }
    public bool Ischeckpoint { get { return checkpointTile; } } //find checkpoint 
    public TileEvent myEvent{ get { return speicalEvent; } } //to name tile object

    [SerializeField] protected int tileIndex;
    [SerializeField] protected Unit currentPlayerOnTile;
    public GameObject tileModel;
    public GameObject tileModelBreak;
  
    [Header("Tile Status")]
    [SerializeField] protected bool checkpointTile;
    [SerializeField] protected bool beenDestory;
    [SerializeField] protected int turnToReset;
    [SerializeField] protected TileEvent speicalEvent;
    //[SerializeField]
    public Item tileItem;
    protected bool isStartingTile { get { return tileIndex == GameManager.Instance.startTile.index; } }
    protected bool isEndingTile { get { return tileIndex == GameManager.Instance.finishTile.index; } }
    protected bool hasSpecialTile { get { return speicalEvent == null ? false : !speicalEvent.canSuspendedEvent; } }
    protected bool allowToPlace { get { return speicalEvent == null ? true : speicalEvent.canPlaceItem; } }
  
    //model tile,old 
    protected Color tileOriginalColor;
    protected Color tileBreakColor =Color.black;
    //new one
    [SerializeField] private GameObject mainTileModel;
    //etc
    [SerializeField] private Unit newPlayer;
    private Renderer tileRender;
    private GameObject itemModelProp;
    //private static int CountDownTurnForResetDestoryTile = 99;
    private static int PushBackLimit = 1;
    private static float itemYAxisOffset = 0.15f;
    private IEnumerator coroutine;
  
     void Awake()
    {
        tileRender = GetComponent<Renderer>();
        if (speicalEvent == null)
            speicalEvent = GetComponent<TileEvent>();
      
        if (tileModel == null || tileModelBreak == null)
            return;

        //check prefab
        if (tileModel)
            tileModel.SetActive(false);

        if (tileModelBreak)
            tileModelBreak.SetActive(false);

        if (speicalEvent && speicalEvent.eventModel != null)
            speicalEvent.eventModel.SetActive(false);

        //model set up
        mainTileModel = TileModelSetup();
        mainTileModel.SetActive(true);
    
    }

    void Start()
    {
        tileOriginalColor = tileRender.material.color; 
        beenDestory = false;
        tileItem = null;
    }
       
    public bool DestoryTile()
    {
        if (!(checkpointTile || isStartingTile || isEndingTile || hasSpecialTile) && currentPlayerOnTile == null)
        {
            beenDestory = true;
            tileItem = null;

            if (itemModelProp)
            {
                Destroy(itemModelProp);
                itemModelProp = null;
            }

            if (speicalEvent && speicalEvent.canRemoveByCannon)
                RemoveEvent();

            TileModelChange();
            GameManager.Instance.PlayTileBreakSound();
            //countdown
            //turnToReset = GameManager.Instance.mainTurn + CountDownTurnForResetDestoryTile;
            //StartTileRestoreCountDown();

            //end event turn >> ending Turn
            return true;
        }
        //Debug.Log("<color=red>Not allow to destory this tile. Please select one.</color>");
        return false;
    }
  
    private void StartTileRestoreCountDown()
    {
        if (coroutine!=null)
            return;

        coroutine = TileRestoreCountDown();
        StartCoroutine(coroutine);
    }
    IEnumerator TileRestoreCountDown()
    {
        Debug.unityLogger.Log(name, "Tile start count down");
        while (GameManager.Instance.mainTurn <= turnToReset)
        {
            yield return null;
        }
        Debug.unityLogger.Log(name, "Tile RESET");
        beenDestory = false;
        coroutine = null;
        TileModelChange();
    }
    public bool SetItem(Item item)
    {
        if (tileItem==null && !beenDestory && !(isStartingTile || isEndingTile) && currentPlayerOnTile == null &&allowToPlace)
        {
               tileItem = item;
               ShowItemModel();
                GameManager.Instance.PlayPlaceitemSound();
                Debug.unityLogger.Log(name, item.name + " added complete.");
                return true;
        }
     
        return false;
    }
    public void RemoveEvent()
    {
        speicalEvent = null;
        if (!mainTileModel)
            return;

        mainTileModel.SetActive(false);
        mainTileModel = TileModelSetup();
        mainTileModel.SetActive(true);
    }
    private void TileModelChange()
    {
        Color newColor = beenDestory ? tileBreakColor : tileOriginalColor;
        if (mainTileModel)
            mainTileModel.SetActive(!beenDestory);
        if (tileModelBreak)
            tileModelBreak.SetActive(beenDestory);
        else
            tileRender.material.SetColor("_BaseColor", newColor);
    }
    private void ShowItemModel() //fix cage item pos ???
    {
        if (tileItem.model == null)
            return;

        itemModelProp = Instantiate(tileItem.model, transform.position,transform.rotation);

        Vector3 modelAngle = tileItem.model.transform.eulerAngles;
        Vector3 tileAngle = transform.eulerAngles;
        Vector3 newPos = new Vector3(transform.position.x, transform.position.y+itemYAxisOffset, transform.position.z);
        itemModelProp.transform.eulerAngles = new Vector3(modelAngle.x+tileAngle.x, modelAngle.y, modelAngle.z+tileAngle.z);
        itemModelProp.transform.position = newPos;
    }
    private GameObject TileModelSetup() //model set up
    {
        return speicalEvent && speicalEvent.eventModel != null ? speicalEvent.eventModel : tileModel;
    }
    private void PushPlayer()
    {
        //Debug.Assert(newPlayer != null, "no new player");
        if (newPlayer == null || (isStartingTile || isEndingTile))
           return;
    
        if (speicalEvent != null && !speicalEvent.canPushback)
           return;

        //check jailed event push , not recommend to do
     
        if(currentPlayerOnTile != newPlayer && currentPlayerOnTile.jailed)
        {
            Tile next = GameManager.Instance.GetTile(tileIndex + 1);
            if (next.currentPlayerOnTile != null)
            {
                Tile nextTile = next;
                nextTile.currentPlayerOnTile.MoveBack(2);
            }

            //for jailed currentplayer
            StartCoroutine(WaitPlayerToPushForward(newPlayer, PushBackLimit));
            //Debug.Log("go back");
            return;
        }

        if (currentPlayerOnTile != newPlayer)
        {
            //Debug.Log("go back");
            currentPlayerOnTile.MoveBack(PushBackLimit);
            currentPlayerOnTile = newPlayer;
        }
      
    }
    IEnumerator WaitPlayerToDoDestoryEvent(Unit player)
    {
        while (player.isSleeping == false)
            yield return null;
        Debug.Log("Destoryed Tile. Back to Start");
        Tile checkpoint = GameManager.Instance.FindNearestCheckPointTile(tileIndex);
        player.WarpTo(checkpoint);
        //restore tile
        beenDestory = false;
        tileItem = null;
        TileModelChange();
    }
    IEnumerator WaitFininshPlayerToStartCheck(Unit player)
    {
        while (player.isSleeping == false)
        {
            yield return null;
        }
        player.CheckValueLeftAfterFinishTile();
    }
    IEnumerator WaitPlayerToStartEvent(Item tileItem, Unit player)
    {
        while (player.isSleeping == false)
           yield return null;
        //Debug.Log(player.name + " start item effect");
        this.tileItem.useItem(player);
        if (this.tileItem.beActivate==true)
        {
            ItemAnimation animte = itemModelProp.GetComponent<ItemAnimation>();
            animte.PlayAnimation();
        }
        this.tileItem = null;
        print(tileItem.timeToReModel);
        StartCoroutine(ItemModelCountDown(tileItem.timeToReModel));
    }
    IEnumerator WaitPlayerToStartEvent(TileEvent sp,Unit player)
    {
        while (player.isSleeping==false)
            yield return null;
        //Debug.Log(player.name + " start the event");
        player.myEvent = sp;
        sp.TileEventActive(player);
     }
    IEnumerator WaitPlayerToPushForward(Unit player,int move)
    {
        while (player.isSleeping == false)
            yield return null;
        player.MoveForward(move);
    }
    IEnumerator ItemModelCountDown(int turn)
    {
        float timeToDestory = 0.0f;
        if (turn>=0)
        {
            if(turn==0)
                timeToDestory = 4.5f;
            turn += GameManager.Instance.mainTurn;
            while (turn > GameManager.Instance.mainTurn)
                yield return null;
        }
        Destroy(itemModelProp,timeToDestory);
        itemModelProp = null;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Unit player = other.GetComponent<Unit>();
            newPlayer = player;
            //Debug.unityLogger.Log(name, newPlayer + " enter");

            if (currentPlayerOnTile == null)
            {
                //Debug.unityLogger.Log(name, "new player on tile " + newPlayer);
                //Debug.Log("new player on tile "+newPlayer);
                currentPlayerOnTile = newPlayer;
            }

            if (newPlayer.targetTile != tileIndex)
                return;
       
                //do pushback
                PushPlayer();

                if (isEndingTile)
                {
                    StartCoroutine(WaitFininshPlayerToStartCheck(newPlayer));
                    return;
                }
             
                if (beenDestory)
                {
                    StartCoroutine(WaitPlayerToDoDestoryEvent(newPlayer));
                    return;
                }

                if (tileItem == null && speicalEvent == null)
                       return;
         
                if (tileItem != null&& newPlayer) //new edit  == GameManager.Instance.player
                {
                    //Debug.Log("trap item active");
                    StartCoroutine(WaitPlayerToStartEvent(tileItem,newPlayer));
                }

                if (speicalEvent != null)
                {
                    //Debug.Log(name + " SpEvent active");
                    StartCoroutine(WaitPlayerToStartEvent(speicalEvent,newPlayer));
               
                }

        }

    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Unit player = other.GetComponent<Unit>();
            if (player == currentPlayerOnTile)
                currentPlayerOnTile = null;
            newPlayer = null;
        }
    }
}
