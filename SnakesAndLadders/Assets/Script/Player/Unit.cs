﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit : MonoBehaviour
{
    public TileEvent myEvent { get { return currentEvent; } set { currentEvent = value; } } 
    public int currentTile { get { return currentIndex; }}
    public bool playable { get { return isPlayable; } }
    public bool trading { get { return isTrading; } set { isTrading = value; } }
    public bool jailed { get { return beJailed; } }
    public bool playAnimation { get { return isPlayAnimation; } }
    public bool isSleeping {get { return gameObject.GetComponent<Rigidbody>().IsSleeping(); } }
    public bool traped { get { return beenTraped; } }
    public int targetTile { get { return nextIndex; } } 
    public List<Item> inventory { get { return myInventory; } }
 
    [SerializeField] protected TileEvent currentEvent;
    [SerializeField] protected int currentIndex; // 0>1, 1>2
    [SerializeField] protected int moveValue;
    public float speed = 1.0f;
    [SerializeField]protected bool isPlayable;
     protected bool beJailed;  
     protected bool isTrading;
     protected bool beenTraped;
    [SerializeField]protected bool isPlayAnimation;
    [SerializeField] private int turnToUnjailed = 0;
    [Header("Items")]
    [SerializeField]protected List<Item> myInventory;
   
    [Header("Animation")]  
    [SerializeField]private Animator unitAnimator;
    private float unitAniDelay =1;
    [SerializeField] private Animator modelAnimator;
    [Header("UI and Popup")]
    [SerializeField] protected GameObject cursor;

    private bool isRolled;
    private int nextIndex;
    private float groundHeightDefault;
    //[SerializeField]private float groundHeightCheck; 
    private TradeItemSystem tradeItemSystem;
    private const float UnitHeightOffset = 0.15f;
    private const float PlayerFaceToYAxis = 180;
    private const int LimitedInventory = 3;
    private IEnumerator coroutineForward;
    private IEnumerator coroutineBack;
    private IEnumerator coroutineTo;
    //event
    //public delegate void PlayerAction();
    //public event PlayerAction StartAct;
    //private void PlayerBegin() { if (StartAct != null) StartAct(); }

    void Start()
    {
        currentIndex = 0;
        nextIndex = currentIndex;
        isRolled = false;
        beJailed = false;
        isTrading = false;
        isPlayable = false;
        isPlayAnimation = false;
        beenTraped = false;
        coroutineForward = null;
        coroutineBack = null;
        coroutineTo = null;
        //get ground height
        tradeItemSystem = FindObjectOfType<TradeItemSystem>();
        Debug.Assert(cursor, name+": missing cursor");
        if (cursor)
            cursor.SetActive(false);
        Debug.Assert(unitAnimator,name +": missing unit animator [hint: model in child's Palyer prefab]");
        if (modelAnimator == null)
            Debug.unityLogger.LogWarning(name,"missing model animator [hint: player_ in child's Palyer prefab]");
     
    }
    void Update()
    {   /* always active */
        bool beIdle = modelAnimator == null ? true : modelAnimator.GetCurrentAnimatorStateInfo(0).IsName("Idle");
        bool SPActive = modelAnimator == null ? true :modelAnimator.GetBool("Speical");
        isPlayAnimation = !(unitAnimator.GetCurrentAnimatorStateInfo(0).IsName("Unit_idle") && beIdle || SPActive);
        //check groundheight
        //float currentlyheight = transform.position.y;
        //groundHeightCheck = currentlyheight - groundHeightDefault;
        //check player state
        if (beJailed && modelAnimator) PlayJailedSPAnimation();
        //reset jailed 
        if (beJailed && GameManager.Instance.mainTurn> turnToUnjailed)
        {
            Debug.Log("reset " + name + " [Unjailed].Ready to start");
            beJailed = false;
            turnToUnjailed = 0;
            if (modelAnimator) PlayerBeIdle();
        }
    
        /*enable to play when this is your turn. */
        if (!isPlayable)
            return;

        if (GameManager.Instance.m_gameStage != GameState.StartTurn && GameManager.Instance.m_gameStage != GameState.EndTurn&&GameManager.Instance.m_gameStage!=GameState.PlayerTurn)
        {
            if (isSleeping == false)
            {
                GameManager.Instance.m_gameStage = GameState.MoveTurn;
                if (modelAnimator&&currentEvent==null) SetRunAnimation(true);
            }
            else
            {
               // GameManager.Instance.m_gameStage = GameState.EventTurn;
                if (modelAnimator) PlayerBeIdle();
            }
        
           return;
        }

    }
    //player state 
    public void BeginTurn()
    {
        if (playable)
            return;
        isPlayable = true;
        //cursor show who is player enable in turn
        if (cursor) cursor.SetActive(isPlayable);
        transform.rotation = Quaternion.Euler(0, 180, 0);
        //groundHeightDefault = transform.position.y;
        //....
    }
    public void FinishTurn()
    {
        isPlayable = false;
        isRolled = false;
        //cursor show who is player enable in turn
        if (cursor) cursor.SetActive(isPlayable);
        //face to camera
        transform.rotation = Quaternion.Euler(0, 180, 0);
        //....
    }
    //dice
    public void PlayerRollDice()
    {
        if (GameManager.Instance.diceValue > 0)
        {
           MoveForward(GameManager.Instance.diceValue);
           GameManager.Instance.m_gameStage = GameState.MoveTurn;
        }
        else
        {
            PlaySadSPAnimation();
           GameManager.Instance.m_gameStage = GameState.EventTurn;
        }
        
        isRolled = true;
    }
    //move
    public void MoveBack(int value)
    {
        if (coroutineBack == null)
        {
            coroutineBack = MoveBackTo(value);
            StartCoroutine(coroutineBack);
        }
    }
    public void MoveForward(int value)
    {
        if (coroutineForward == null)
        {
            coroutineForward = MoveForwardTo(value);
            StartCoroutine(coroutineForward);
        }
    }
    public void MoveTo(Vector3 target, int index, float speed)
    {
        if (coroutineTo == null)
        {
            coroutineTo = Move(target, index, speed);
            StartCoroutine(coroutineTo);
        }
    }
    private IEnumerator MoveBackTo(int value)
    {
         nextIndex = currentIndex - value;
         moveValue = value;
        //Debug.Log(name+"go back "+nextIndex+" tile");
         while (currentIndex > nextIndex && currentIndex > 0)
        {
            Transform tile = GameManager.Instance.GetTile(currentIndex - 1).transform;
            Vector3 nextTile = new Vector3(tile.transform.position.x, tile.transform.position.y + UnitHeightOffset, tile.transform.position.z);
            transform.LookAt(nextTile, Vector3.up);
            transform.position = Vector3.MoveTowards(transform.position, nextTile, speed * Time.deltaTime);

            if (transform.position == nextTile)
            {
                currentIndex--;
                moveValue--;
                //Debug.unityLogger.Log(name, moveValue);
            }
            yield return null;
        }
        // Debug.Log("End Moveback "+name);
        //transform.rotation = Quaternion.Euler(0, 180, 0);
        GameManager.Instance.m_gameStage = GameState.EventTurn;
        nextIndex = currentIndex;
        coroutineBack= null;
    }
    private IEnumerator MoveForwardTo(int value)
    {
        nextIndex = currentIndex + value;
        moveValue = value;
        if (nextIndex > GameManager.Instance.finishTile.index)
              nextIndex = GameManager.Instance.finishTile.index;
        //Debug.Log(name+"go back "+nextIndex+" tile");
        while (currentIndex < nextIndex && currentIndex < GameManager.Instance.finishTile.index)
        {
            Transform tile = GameManager.Instance.GetTile(currentIndex+1).transform;
            Vector3 nextTile = new Vector3(tile.transform.position.x, tile.transform.position.y + UnitHeightOffset, tile.transform.position.z);
            transform.LookAt(nextTile,Vector3.up);
            transform.position = Vector3.MoveTowards(transform.position, nextTile, speed * Time.deltaTime);
            if (transform.position == nextTile)
            {
                currentIndex++;
                moveValue--;
               // Debug.unityLogger.Log(name, moveValue);
            }
            yield return null;
        }
        // Debug.Log("End Moveback "+name);
        //transform.rotation = Quaternion.Euler(0, 180, 0);
        GameManager.Instance.m_gameStage = GameState.EventTurn;
        nextIndex = currentIndex;
        coroutineForward = null;
    }
    private IEnumerator Move(Vector3 target, int index, float sailSpeed)
    {
        nextIndex = index;
        Vector3 nextTarget = new Vector3(target.x, target.y + UnitHeightOffset, target.z);
        while (transform.position != nextTarget)
        {
            transform.LookAt(nextTarget, Vector3.up);
            transform.position = Vector3.MoveTowards(transform.position, nextTarget, sailSpeed * Time.deltaTime);
            yield return null;
        }
        GameManager.Instance.m_gameStage = GameState.EventTurn;
        Debug.Log("Finish Move to "+index);
        currentIndex = index;
        //nextIndex = currentIndex;
        //currentIndex = nextIndex;
        coroutineTo = null;
    }
    private IEnumerator WarpTo(Transform target, int index)
    {
        isPlayAnimation = true;
        Vector3 targetPos = new Vector3(target.position.x, target.position.y + UnitHeightOffset, target.position.z);
        //start warp
        unitAnimator.SetBool("Destoryed", true);
        //new warp area
        yield return new WaitForSeconds(unitAniDelay);
        transform.position = targetPos;
        unitAnimator.SetBool("Destoryed", false);
       //end animation,end
        yield return new WaitForSeconds(unitAniDelay);
        transform.rotation = Quaternion.Euler(0, 180, 0);
        isPlayAnimation = false;
        currentIndex = index;
    }
    //player data setup
    internal Vector3 GetUnitPosition(Vector3 pos)
    {
        return new Vector3(pos.x, pos.y + UnitHeightOffset, pos.z);
    }
    internal void TileIndexSetup(Tile target)
    {
        nextIndex = target.index;
        currentIndex = target.index;
    }
    internal void JailPlayer(int timetoReset)
    {
        beJailed = true;
        turnToUnjailed = (GameManager.Instance.mainTurn) + timetoReset;
    }
    internal void WarpTo(Tile targetTile)
    {
        StopAllCoroutines();
        coroutineTo = null;
        coroutineForward = null;
        coroutineBack = null;
        TileIndexSetup(targetTile);
        StartCoroutine(WarpTo(targetTile.transform, targetTile.index));
    }
    internal void CheckValueLeftAfterFinishTile()
    {
        //Debug.unityLogger.Log(name, "check value left.");
        if (moveValue == 0)
            return;
        Debug.unityLogger.Log(name, "move back " + moveValue);
        MoveBack(moveValue);
        GameManager.Instance.m_gameStage = GameState.MoveTurn;
    }
    //items
    internal bool GetItem(Item item)
    {
        if (myInventory.Count == 3)
        {
            Debug.unityLogger.Log(name, "<color=red>Inventory is full</color>");
            tradeItemSystem.StartTradeItem(item,this);
            return false;
        }
        else
        {
            Debug.unityLogger.Log(name, "<color=green>item added</color>");
            myInventory.Add(item);
            return true;
        }
    }
    internal bool UseItemCounter(int counterID,int itemID)
    {
        bool invertoryIsEmpty = myInventory.Count > 0 ? true : false;
        if (invertoryIsEmpty == true)
            foreach(Item item in myInventory)
            {
                if (item.id == counterID)
                {
                    Debug.unityLogger.Log("[Item effect clear!!]");
                    item.useItem(this);
                    PlaySuccessAnimation();
                    myInventory.Remove(item);
                    return true;
                }
                else continue;
            }
      
        if(itemID!=3)
           PlayFallOnGroundAnimation();
        StartCoroutine("TrapCount");
        return false;
    }
    IEnumerator TrapCount()
    {
        beenTraped = true;
        yield return new WaitForSeconds(3);
        beenTraped = false;
    }
    //animation
    private void PlayerBeIdle()
    {
        bool state = false;
        SetRunAnimation(state);
        SetOnAirAnimation(state);
        SetSpeicalAnimation(state, 0);
    }
    private void SetSpeicalAnimation(bool state,int value)
    {
        modelAnimator.SetInteger("SpeicalType", value);
        modelAnimator.SetBool("Speical", state);
    }
    private void SetRunAnimation (bool state)
    {
       modelAnimator.SetBool("Run", state);
    }
    private void SetOnAirAnimation(bool state)
    {
       modelAnimator.SetBool("onAir", state);
    }
    public void PlayOnAirAnimation(bool state)
    {
        if (!modelAnimator)
            return;
        SetOnAirAnimation(state);
    }
    public void PlayGetItemAnimation() { if (modelAnimator) modelAnimator.SetTrigger("GetItem"); }
    private void PlayFallOnGroundAnimation() { if (modelAnimator) modelAnimator.SetTrigger("FallDown"); }
    private void PlaySuccessAnimation() { if (modelAnimator) modelAnimator.SetTrigger("Win"); }
    private void PlayJumpAnimation() { if (modelAnimator) modelAnimator.SetTrigger("Jump"); } 
    private void PlayJailedSPAnimation() { if (modelAnimator) SetSpeicalAnimation(true, 0); }
    public void PlaySadSPAnimation() { if (modelAnimator) SetSpeicalAnimation(true, 1); } 
    public void PlayWinSPAnimation() 
    {
        if (modelAnimator == null)
            return; 

        if (Random.value < 0.5f)
            SetSpeicalAnimation(true, 2);
        else
            SetSpeicalAnimation(true, 3);
    }
}
