﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateManager : MonoBehaviour
{
    public Animator animate;
    public bool run;
    public bool onAir;
    public bool speicalClip;
    public int speicalType = 0;
   
    private void Start()
    {
        run = false;
        onAir = false;
        speicalClip = false;
        speicalType = 0;

    }

    private void Update()
    {
        //player.playAnimation = animate.GetCurrentAnimatorStateInfo(0).IsName("Idle") ? false : true;

        if (Input.GetKeyDown(KeyCode.Alpha1))
            run = !run;
        if (Input.GetKeyDown(KeyCode.Alpha2))
            PlayJumpClip();
        if (Input.GetKeyDown(KeyCode.Alpha3))
            onAir = !onAir;
        if (Input.GetKeyDown(KeyCode.Alpha4))
            PlayGetItemClip();
        if (Input.GetKeyDown(KeyCode.Alpha5))
            PlayFallDownClip();
        if (Input.GetKeyDown(KeyCode.Alpha6))
            PlayWinClip();
        if (Input.GetKeyDown(KeyCode.Alpha0))
            speicalClip =! speicalClip; 

        SetRunClip(run);
        SetOnAirClip(onAir);
        SetSpeicalClip(speicalClip, speicalType);
        //if (animate.GetCurrentAnimatorStateInfo(0).IsName("Item")) //need to fixed it
        //    Debug.Log("get item animation ");
    }
  
    public void SetRunClip(bool set)
    {
        animate.SetBool("Run", set);
    }
    public void SetOnAirClip(bool set)
    {
        animate.SetBool("onAir", set);
    }
    public void SetSpeicalClip(bool set,int value)
    {
        animate.SetInteger("SpeicalType", value);
        animate.SetBool("Speical", set);
    }
    public void PlayJumpClip()
    {
        animate.SetTrigger("Jump");
    }
    public void PlayGetItemClip()
    {
       animate.SetTrigger("GetItem");
    }
    public void PlayFallDownClip()
    {
        animate.SetTrigger("FallDown");
    }
    public void PlayWinClip()
    {
        animate.SetTrigger("Win");
    }
   
}
