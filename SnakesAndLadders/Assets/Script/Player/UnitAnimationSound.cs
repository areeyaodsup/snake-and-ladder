﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class UnitAnimationSound : MonoBehaviour
{
    Animator animator;
    AudioSource audioSound;
    public AudioClip[] footStepRight;
    public AudioClip[] footStepLeft;
    public AudioClip failedClip;
    public AudioClip suscessClip;
    private void Start()
    {
        animator = this.GetComponent<Animator>();
        audioSound = this.GetComponent<AudioSource>();
        audioSound.outputAudioMixerGroup = SoundManager.Instance.Mixer.FindMatchingGroups("SFX")[1];
        if (audioSound.isPlaying)
            audioSound.Stop();
    }
    
    void SoundDisable()
    {
        if (audioSound.isPlaying)
            audioSound.Stop();
    }
    void PlayerStepLeft()
    {
        audioSound.PlayOneShot(RandomAudioClip(footStepLeft));
    }
    void PlayerStepRight()
    {
        audioSound.PlayOneShot(RandomAudioClip(footStepLeft));
    }
    void PlayerFailed()
    {
        audioSound.PlayOneShot(failedClip);
    }
    void PlayerSuscess()
    {
        audioSound.PlayOneShot(suscessClip);
    }
    AudioClip RandomAudioClip(AudioClip[] audioClips)
    {
        return audioClips[Random.Range(0, audioClips.Length)];
    }
   
}
