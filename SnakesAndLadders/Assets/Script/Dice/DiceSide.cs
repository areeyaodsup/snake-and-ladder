﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiceSide : MonoBehaviour
{
    public bool onGround {get { return isOnGround; } }
    [SerializeField]
    protected bool isOnGround;
    public void Start()
    {
        isOnGround = false;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("GroundDice"))
            isOnGround = true;
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("GroundDice"))
            isOnGround = false;
    }
}
