﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dice : MonoBehaviour
{
    public int diceNumber { get { return diceValue; } }
    public bool rolling { get { return isRolling; } }
    [SerializeField] protected DiceSide[] diceSides;
    [SerializeField] protected int diceValue;
    [SerializeField] protected bool isRolling;

    private int groundValue = 0;
    [SerializeField]private float force = 0.5f;
    private bool onGround;
    private Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        onGround = false;
        isRolling = false;
    }

    public void RollDice()
    {
        Vector3 pushUp = new Vector3(0, force, 0);
        Vector3 randomTorque = new Vector3(Random.Range(1, 500), Random.Range(1, 500), Random.Range(1, 500));
        rb.AddForce(pushUp, ForceMode.Impulse);
        rb.AddRelativeTorque(randomTorque, ForceMode.Impulse);
        StartCoroutine("CheckDiceRoll");
    }
    public void RollDice(Transform diceSpawner)
    {
        transform.position = diceSpawner.position;
        Vector3 randomTorque = new Vector3(Random.Range(1, 500), Random.Range(1, 500), Random.Range(1, 500));
        rb.AddForce(diceSpawner.right * force, ForceMode.Impulse);
        rb.AddRelativeTorque(randomTorque, ForceMode.Impulse);
        StartCoroutine("CheckDiceRoll");
    }

    IEnumerator CheckDiceRoll()
    {
        isRolling = true;
        while (!rb.IsSleeping())
        {
            yield return null;
        }

        for (int i = 0; i < diceSides.Length; i++)
        {
            onGround = diceSides[i].onGround;
            if (onGround)
            {
                groundValue = i;
                break;
            }
        }

        isRolling = false;

        switch (groundValue + 1)
        {
            case 1: diceValue = 6; break;
            case 2: diceValue = 5; break;
            case 3: diceValue = 4; break;
            case 4: diceValue = 3; break;
            case 5: diceValue = 2; break;
            case 6: diceValue = 1; break;
        }
        // Debug.Log("Roll Dice Complete");
        Debug.Log("< " + diceValue + " >");
    }
}
