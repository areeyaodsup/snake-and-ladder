﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Cheat : MonoBehaviour
{
    public InputField numberfield;
    public Dropdown itemdropdown;
    public Item[] items;
    private Unit player;
    [SerializeField]private Item givenItem;
    public void Start()
    {
        numberfield.text = "0";
        itemdropdown.onValueChanged.AddListener(delegate { DropDownItemSelect(itemdropdown); });
        givenItem = items[0];
    }
    public void MoveForwardCheat()
    {
        int number = int.Parse(numberfield.text);
        if (number == 0)
        {
            GameManager.Instance.m_gameStage = GameState.EventTurn;
            return;
        }
        player = GameManager.Instance.player;
        player.MoveForward(number);
        GameManager.Instance.m_gameStage = GameState.MoveTurn;
    }   

    public void MoveBackwardCheat()
    {
        int number = int.Parse(numberfield.text);
        if (number == 0)
        {
            GameManager.Instance.m_gameStage = GameState.EventTurn;
            return;
        }
        player = GameManager.Instance.player;
        player.MoveBack(number);
        GameManager.Instance.m_gameStage = GameState.MoveTurn;
    }

    public void DropDownItemSelect(Dropdown dropdown)
    {
        switch (dropdown.value)
        {
            case 0: givenItem =items[0];
                break;
            case 1: givenItem = items[1];
                break;
      
        }
    }

    public void AddItemToPlayer()
    {
        player = GameManager.Instance.player;
        player.inventory.Add(givenItem);
        GameManager.Instance.UpdateInventoryUI();
        Debug.Log("give " + givenItem.name + " to " + player.name);
    }

    public void RemovePlayerItem()
    {
        player = GameManager.Instance.player;
        if (player.inventory.Count > 0)
        {
            player.inventory.RemoveAt(0);
            GameManager.Instance.ResetInventoryUI();
            GameManager.Instance.UpdateInventoryUI();
        }
        Debug.Log("remove 1 item  in" + player.name+" inventory");
    }
    public void RemoveAllItem()
    {
        player = GameManager.Instance.player;
        player.inventory.Clear();
        GameManager.Instance.ResetInventoryUI();
        Debug.Log( player.name + " inventory clear");
    }
}
