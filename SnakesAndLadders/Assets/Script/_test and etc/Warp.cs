﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Warp : MonoBehaviour
{
    public Animator anima;
    public Animation a;
    public Transform warpTarget;
    public float sec;
    public bool active = false;
      // Start is called before the first frame update
    void Start()
    {
    
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            warpTarget.transform.position = new Vector3(Random.Range(-7, 7), 0, Random.Range(-7, 7));
            StartCoroutine("startAniamtion");
        }
        //active = !active;
        active = GetComponent<Rigidbody>().IsSleeping();
        //anima.SetBool("Destoryed", active);
    }

    IEnumerator startAniamtion()
    {
        anima.SetBool("Destoryed", true);
        yield return new WaitForSeconds(sec);
        transform.position = warpTarget.position;
        anima.SetBool("Destoryed", false);
    }
}
