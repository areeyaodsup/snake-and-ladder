﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomScript : MonoBehaviour
{
    public int randomLoop = 100;
    int[] value = {0,1,2,3,4,5,6,7,8,9 };
    float[] freq = { 0.1f, 0.1f, 0.1f, 0.1f, 0.1f, 0.1f, 0.1f,0.1f,0.1f,0.1f };

    WeightedRandomBag<String> itemDrops;
    // Start is called before the first frame update
    void Start()
    {
        itemDrops = new WeightedRandomBag<String>();

        itemDrops.AddEntry("10 Gold", 5.0);
        itemDrops.AddEntry("Sword", 20.0);
        itemDrops.AddEntry("Shield", 45.0);
        itemDrops.AddEntry("Armor", 20.0);
        itemDrops.AddEntry("Potion", 10.0);

    }

    // Update is called once per frame
    void Update()
    {
        //if (Input.GetKeyDown(KeyCode.Space))
        //{
        //    int value = 0;
        //    for (int i = 0; i < randomLoop; i++)
        //    {
        //        // value = RandomDiceValue();
        //        // Debug.Log(i+" : "+value);
        //        Debug.Log(itemDrops.GetRandom());
        //    }
        //}
           // RandomDiceValue();
    }

    //public int RandomDiceValue()
    //{
    //    int num = 0;
    //    return num;
    //}

    class WeightedRandomBag<T>
    {
        private struct Entry
        {
            public double accumulatedWeight;
            public T item;
        }

        private List<Entry> entries = new List<Entry>();
        private double accumulatedWeight;
        private System.Random rand = new System.Random();

        public void AddEntry(T item,double weight)
        {
            accumulatedWeight += weight;
            Debug.Log(item + " " + accumulatedWeight);
            entries.Add(new Entry { item = item, accumulatedWeight = accumulatedWeight });
        }

        public T GetRandom()
        {
            double r = rand.NextDouble() * accumulatedWeight;

            foreach(Entry entry in entries)
            {
                if (entry.accumulatedWeight >= r)
                    return entry.item;
            }
            return default(T);
        }

    }
}
