﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GodHand : MonoBehaviour
{
    public Vector3 worldPos;
    public Transform point;
  
    // Update is called once per frame
    void Update()
    {
       
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hitInfo;


        if(Input.GetMouseButtonDown(0)&& Physics.Raycast(ray, out hitInfo))
        {
            worldPos = hitInfo.point;
            point.transform.position = worldPos;
            Debug.Log(worldPos);
         
        }
    }
}
