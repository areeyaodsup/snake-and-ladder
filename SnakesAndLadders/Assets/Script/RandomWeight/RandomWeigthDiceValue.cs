﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomWeigthDiceValue : MonoBehaviour
{
    public int totalDices = 1;
    public int loop = 1;
    public bool showDetail = false;
    public List<RandomWeight> valueList = new List<RandomWeight>();
    private int totalValue;
    private RandomWeightScript<int> diceValues;
    [System.Serializable]
    public class RandomWeight
    {
        public int diceValue;
        public double weight;
    }
    public void Start()
    {
        AddData();
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
            AddData();

        if (Input.GetKeyDown(KeyCode.Space))
            GetDiceValue();
  
    }

    public void GetDiceValue()
    {
      for(int i = 0; i < loop; i++)
        {
            int value = 0;
            string show = "";
            totalValue = 0;
            for (int j = 0; j < totalDices; j++)
            {
                value = diceValues.GetRandom();
                totalValue += value;
                show = show + "" + value.ToString();
            }
            if(showDetail)
                    Debug.Log("["+show+"]");
            Debug.Log("Total : " + totalValue);
        }
    }

    private void AddData()
    {
        diceValues = new RandomWeightScript<int>();
        foreach(RandomWeight dice in valueList)
        {
            diceValues.AddEntry(dice.diceValue, dice.weight);
        }
    }

}
