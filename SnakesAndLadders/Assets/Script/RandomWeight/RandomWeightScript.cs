﻿using System.Collections;
using System.Collections.Generic;
using System;

public class RandomWeightScript<T>  
{
    private struct Entry
    {
        public double accumulatedWeigth;
        public T thing;
    }

    private List<Entry> enties = new List<Entry>();
    private double accumulatedWeight;
    private Random rand = new Random();

    public void AddEntry(T thing, double weight)
    {
        accumulatedWeight += weight;
        enties.Add(new Entry { thing =thing,accumulatedWeigth=accumulatedWeight});
    }

    public T GetRandom()
    {
        double r = rand.NextDouble() * accumulatedWeight;

        foreach(Entry entry in enties)
        {
            if (entry.accumulatedWeigth >= r)
                return entry.thing;
        }
        return default(T);
    }
}
