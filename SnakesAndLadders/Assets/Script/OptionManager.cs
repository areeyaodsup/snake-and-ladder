﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionManager : MonoBehaviour
{
    public Slider musicSoundBar;
    public Slider SFXSoundBar;
    public Toggle diceAnimationEnable;
    private float minSliderValue = 0.0001f;
    private float maxSliderValue = 1f;
  
    void Start()
    {
        diceAnimationEnable.onValueChanged.AddListener(delegate { DiceEnable(diceAnimationEnable); });
        musicSoundBar.onValueChanged.AddListener(delegate { SetMusic(musicSoundBar); });
        SFXSoundBar.onValueChanged.AddListener(delegate { SetSFXSound(SFXSoundBar); });

        musicSoundBar.maxValue = maxSliderValue;
        musicSoundBar.minValue = minSliderValue;
        SFXSoundBar.maxValue = maxSliderValue;
        SFXSoundBar.minValue = minSliderValue;
        diceAnimationEnable.isOn = GameManager.rollDiceEnable;
        //Debug.Log(SoundManager.Instance.MusicVolumeDefault);

        musicSoundBar.value = Mathf.Pow(10, SoundManager.Instance.MusicVolumeDefault / 20);
        SFXSoundBar.value = Mathf.Pow(10, SoundManager.Instance.MasterSFXVolumeDefault / 20);
    }

    private void OnEnable()
    {
        diceAnimationEnable.isOn = GameManager.rollDiceEnable;
    }
    void SetMusic(Slider scrollbar) 
    {
        SoundManager.Instance.MusicVolume = Mathf.Log10( scrollbar.value)*20;
        //Debug.Log("Music " + Mathf.Log10(scrollbar.value) * 20);
    }
    void SetSFXSound(Slider scrollbar)
    {
        SoundManager.Instance.MasterSFXVolume = Mathf.Log10( scrollbar.value)*20;
    }
    void DiceEnable(Toggle toggle)
    {
        if (toggle.isOn)
        {
            Debug.Log("dice animation Enable");
            GameManager.rollDiceEnable = true;
        }
        else
        {
            Debug.Log("dice animation Disable");
            GameManager.rollDiceEnable = false;
        }
    }
    private void OnDisable()
    {
        SoundManager.Instance.AudioSoundVolumeUpdate();
    }
}
