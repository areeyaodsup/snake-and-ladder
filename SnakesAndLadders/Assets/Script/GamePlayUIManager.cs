﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class GamePlayUIManager : MonoBehaviour
{
    public Button optionMainButton;
    public Button backToMenuButton;
    public Button mapButton;
    public string menuScene = "";
    [Header("OptionUI setting")]
    public GameObject optionCanvasUI;
    public Button optionButton;
    private bool openOptionWindow;
    [Header("TradeItem setting")]
    public Button exitButton;
    public Button acceptButton;
    [Header("Sound Setting")]
    AudioSource audioSource;
    public AudioClip OnClickButtonSound;
    // Start is called before the first frame update
    void Start()
    {
        audioSource = this.GetComponent<AudioSource>();
        audioSource.outputAudioMixerGroup = SoundManager.Instance.Mixer.FindMatchingGroups("UI")[0];
        //add sound
        optionMainButton.onClick.AddListener(() => OnClickSound());
        backToMenuButton.onClick.AddListener(() => OnClickSound());
        optionButton.onClick.AddListener(() => OnClickSound());
        exitButton.onClick.AddListener(() => OnClickSound());
        acceptButton.onClick.AddListener(() => OnClickSound());
        mapButton.onClick.AddListener(() => OnClickSound());
        //add event
        optionMainButton.onClick.AddListener(() => OptionButtonClick());
        backToMenuButton.onClick.AddListener(() => BackToMainMenu());
        optionButton.onClick.AddListener(()=>OptionUIDisable());
     
        openOptionWindow = false;
        optionCanvasUI.SetActive(openOptionWindow);
    }
    void OnClickSound()
    {
        if (OnClickButtonSound == null)
            return;
        if (audioSource.isPlaying) 
            audioSource.Stop();
        audioSource.PlayOneShot(OnClickButtonSound);
    }
    void OnClickSound(AudioClip sound)
    {
        if (OnClickButtonSound == null)
            return;
        if (audioSource.isPlaying)
            audioSource.Stop();
        audioSource.PlayOneShot(sound);
    }
    void OptionButtonClick()
    {
        if (openOptionWindow == false)
        {
            OptionUIEnable();
        }
        else
        {
            OptionUIDisable();
        }
    }
    void OptionUIEnable()
    {
        openOptionWindow = !openOptionWindow;
        optionCanvasUI.SetActive(true);
    }
    void OptionUIDisable()
    {
        openOptionWindow = !openOptionWindow;
        optionCanvasUI.SetActive(false);
    }
    void BackToMainMenu()
    {
        SoundManager.Instance.VolumeFadeOut();
        Invoke("LoadScene", 2);
    }
    void LoadScene()
    {
        SceneManager.LoadScene(menuScene);
    }
}
