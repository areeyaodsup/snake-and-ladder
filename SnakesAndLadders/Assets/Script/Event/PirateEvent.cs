﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PirateEvent : TileEvent
{
    [Header("Special Setting")]
    [SerializeField] protected Tile targetTile;
    protected GameObject CannonPrefab;
    public AnimationCurve speedCurve;
    public float speedMultiplier = 0.25f;
    public float curveHeight = 2;
    private float yOffset = -1.5f;
    private Unit player;
    private ParabolaCurve parabola = new ParabolaCurve();
    public void Start()
    {
        //name = "Pirate path";
        suspendedEvent = false;
        allowToPushBack = true;
        availableToPlaceItem = false;
        removeByCannon = false;

       if(targetTile==null)
            Debug.unityLogger.LogError(name, "Missing target tile");
        CannonPrefab = GameManager.Instance.cannon;
    }
    public override void TileEventActive(Unit player)
    {
        this.player = player;
        Debug.Log("<color=red>Oh,no. You found the Pirate back to </color>"+targetTile.index+"<color=red> tile.</color>");
        GameObject cannon = Instantiate(CannonPrefab, transform.position, Quaternion.identity);
        cannon.transform.LookAt(targetTile.transform,Vector3.up);
        Destroy(cannon, 5f);
        StartCoroutine("MoveAlongCurve");
    }
    IEnumerator MoveAlongCurve()
    {
        Vector3 playerHidePos = transform.position;
        playerHidePos.y = playerHidePos.y + yOffset;
        player.transform.position = playerHidePos;
        yield return new WaitForSeconds(2f);
        float progress = 0;
        float curveAmount = speedCurve.Evaluate(progress);
        Vector3 playerPos = Vector3.zero;
        //player setup
        player.TileIndexSetup(targetTile);
        player.PlayOnAirAnimation(true); //hardcode
        player.transform.LookAt(targetTile.transform.position, Vector3.up);
        while (progress<= 1)
        {
            progress += Time.deltaTime * speedMultiplier;
            curveAmount = speedCurve.Evaluate(progress);
            playerPos = parabola.SampleParabola(transform.position, targetTile.transform.position, curveHeight, curveAmount); //++++
            player.transform.position = player.GetUnitPosition(playerPos);
            yield return null;
        }
        //player setup
        player.transform.position = player.GetUnitPosition(targetTile.transform.position);
        player.transform.rotation = Quaternion.Euler(new Vector3(0, 180, 0));
        player.PlayOnAirAnimation(false); //hard code
        player.myEvent = null;
        //Debug.Log("End process.Complete parabola curve.");
    }
    private void OnDrawGizmos()
    {
        if (targetTile == null)
            return;

        //normal line draw
        Vector3 targetPos = targetTile.transform.position;
        Vector3 newPos = new Vector3(targetPos.x, targetPos.y + 0.25f, targetPos.z);
        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position,targetPos);
        Gizmos.DrawCube(newPos, new Vector3(0.25f, 0.25f, 0.25f));

        //draw height;
        Vector3 heightPoint = transform.position + (targetPos - transform.position) * 0.5f;
        Vector3 newHeight = heightPoint;
        Vector3 cubeVolume = new Vector3(0.1f, 0.1f, 0.1f);
        newHeight.y = newHeight.y + curveHeight;
        Gizmos.color = Color.black;
        Gizmos.DrawCube(heightPoint, cubeVolume);
        Gizmos.DrawCube(newHeight, cubeVolume);
        Gizmos.DrawLine(heightPoint, newHeight);
        
        //draw parabola
        Gizmos.color = Color.yellow;
        int node = 20;
        Vector3 lineStart = transform.position;
        for (int i = 0; i <= node; i++)
        {
            float cal = i / (float)node;
            Vector3 lineEnd = parabola.SampleParabola(transform.position, targetPos, curveHeight, cal);
            Gizmos.DrawLine(lineStart, lineEnd);
            lineStart = lineEnd;
        }
    }
}
