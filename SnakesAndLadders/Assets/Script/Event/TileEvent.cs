﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Tile))]
public abstract class TileEvent : MonoBehaviour  
{
    public string eventName { get { return eventname; } }
    public bool canSuspendedEvent { get { return suspendedEvent; } }
    public bool canPushback { get { return allowToPushBack; } }
    public bool canPlaceItem { get { return availableToPlaceItem; } }
    public bool canRemoveByCannon { get { return removeByCannon; } }

     [SerializeField] protected string eventname ="SP event";
      public GameObject eventModel = null;
     [SerializeField] protected bool suspendedEvent;
     [SerializeField] protected bool allowToPushBack;
     [SerializeField] protected bool availableToPlaceItem;
     [SerializeField] protected bool removeByCannon;
  
     public abstract void TileEventActive(Unit player);
}
