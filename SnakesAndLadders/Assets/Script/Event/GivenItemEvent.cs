﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GivenItemEvent : TileEvent
{
    [Header("Special Setting")]
    public Item item;
    [SerializeField]private GameObject targethit;
    [SerializeField] private bool eventEnable = false;
    private GameObject cursorInstance;
    private GameObject ItemInstance;
    private float animationDelay = 3.0f; //sec
    private LayerMask layerMask;
    private int tartgetMask;
    private Unit player;
    private Tile tile;
     
    public override void TileEventActive(Unit player)
    {
        //only current player turn can active event
        if (player != GameManager.Instance.player || item == null)
        {
            player.myEvent = null;
            return;
        }
        this.player = player;
        StartCoroutine("MyEventActive");
    }
    void Start()
    {
        //name = "Mystery item";
        suspendedEvent = true;
        allowToPushBack = true;
        availableToPlaceItem = false;
        removeByCannon = true;
        tartgetMask = LayerMask.GetMask("Tile");
        layerMask = GameManager.Instance.selectMask;
        tile = GetComponent<Tile>();
        if (item == null)
            Debug.unityLogger.LogError(name, "Missing item");
    }
    void Update()
    {
        if (!eventEnable)
            return;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hitInfo;
        if (Physics.Raycast(ray, out hitInfo, Mathf.Infinity, tartgetMask))
        {
            //cursorInstance.transform.position = hitInfo.point;
            targethit = hitInfo.transform.gameObject;
            Vector3 hitpoint = targethit.transform.position;
            hitpoint.y = hitInfo.point.y;
            cursorInstance.transform.position =hitpoint ;
        } else if (Physics.Raycast(ray, out hitInfo, Mathf.Infinity, layerMask))
        {
            cursorInstance.transform.position = hitInfo.point;
            targethit = null;
        }
        if (Input.GetMouseButtonDown(0) == false)
            return;

        if (targethit == null)
        {
            Debug.Log("Please select Tile to place item.");
            GameManager.Instance.WriteHelpText("Please select Tile to place item.");
            return;
        }
        Tile t = targethit.GetComponent<Tile>();
           //add item to tile
        if (t.SetItem(item))
        {
            eventEnable = false;
            player.myEvent = null;
            Destroy(cursorInstance, 0.1f);
            item = null;
            tile.RemoveEvent();
            this.enabled = false;
            GameManager.Instance.WriteHelpText("", false);
            CameraManager.Instance.StartCamera();
        }
        else
        {
            Debug.Log("<color=red>Not allow to place this tile. Please select one.</color>");
            //GameManager.Instance.WriteHelpText("Not allow to place this tile.Please select one.");
            GameManager.Instance.WriteHelpErrorText("Not allow to destory this tile. Please select one.");
        }
    }
    private IEnumerator MyEventActive()
    {
        Vector3 itemSpawn = transform.position;
        player.PlayGetItemAnimation();
        if(item.AnimationModel)
            ItemInstance = Instantiate(item.AnimationModel,itemSpawn,Quaternion.identity);
        yield return new WaitForSeconds(animationDelay);
        if (item.AnimationModel)
            ItemInstance.SetActive(false);
        if (item.instantItem)//item.usedNow
        {
            targethit = null;
            eventEnable = true;
            cursorInstance = Instantiate(GameManager.Instance.cursorPrefab);
            GameManager.Instance.WriteHelpText("Select tiles to place item. ", true);
            CameraManager.Instance.SelectStageCamera();
        }
        else
        {
            if (player.GetItem(item) == true)
            {
                item = null;
                tile.RemoveEvent();
                this.enabled = false;
            }
            player.myEvent = null;
            CameraManager.Instance.NormalStageCamera();
        }
    }
}
