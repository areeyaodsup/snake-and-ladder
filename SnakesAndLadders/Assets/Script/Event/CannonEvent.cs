﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonEvent : TileEvent
{
    [Header("Special Setting")]
    public GameObject targethit;
    private GameObject cursorInstance;
    private LayerMask layerMask;
    private int targetMask;
    private Unit player;
    [SerializeField]private bool eventEnable = false;
   public override void TileEventActive(Unit player)
    {
        //only current player turn can active event
        if (player != GameManager.Instance.player)
        {
            player.myEvent = null;
            return;
        }
        this.player = player;
        targethit = null;
        eventEnable = true;
        cursorInstance = Instantiate(GameManager.Instance.cursorPrefab);
        CameraManager.Instance.SelectStageCamera();
        Debug.Log("Cannon Event: Breaking a tile by select and click. ");
        GameManager.Instance.WriteHelpText("Select tiles to break or destory special tiles. (mystery-tile)", true);
    }
    public void Start()
    {
        //name = "Cannon event";
        suspendedEvent = false;
        allowToPushBack = true;
        availableToPlaceItem = false;
        removeByCannon = false;

        targetMask = LayerMask.GetMask("Tile");
        layerMask = GameManager.Instance.selectMask;
    }
    private void Update()
    {
        if (!eventEnable)
            return;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hitInfo;
        if(Physics.Raycast(ray,out hitInfo, Mathf.Infinity,targetMask))
        {
            //Debug.Log("Ray");
            targethit = hitInfo.transform.gameObject;
            Vector3 hitpoint = targethit.transform.position;
            hitpoint.y = hitInfo.point.y;
            cursorInstance.transform.position = hitpoint;
        }
        else if(Physics.Raycast(ray,out hitInfo, Mathf.Infinity, layerMask))
        {
            //Debug.Log(hitInfo.transform.gameObject.name+""+hitInfo.point);
            cursorInstance.transform.position = hitInfo.point;
            targethit = null;
        }
        if (Input.GetMouseButtonDown(0) == false)
            return;
        if (targethit == null)
        {
            Debug.Log("Please select Tile");
            GameManager.Instance.WriteHelpText("Please select tile again.");
            return;
        }
        targethit.GetComponent<Renderer>().material.color = Color.black;
        Tile t = targethit.GetComponent<Tile>();
        if (t.DestoryTile())
        {
            eventEnable = false;
            player.myEvent = null;
            Destroy(cursorInstance, 0.1f);
            GameManager.Instance.WriteHelpText("", false);
            CameraManager.Instance.StartCamera();
        }
        else
        {
            Debug.Log("<color=red>Not allow to destory this tile. Please select one.</color>");
           // GameManager.Instance.WriteHelpText("Not allow to destory this tile. Please select one.");
            GameManager.Instance.WriteHelpErrorText("Not allow to destory this tile. Please select one.");
        }

    }

}
