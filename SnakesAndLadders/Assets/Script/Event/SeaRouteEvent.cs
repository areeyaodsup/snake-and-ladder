﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeaRouteEvent : TileEvent
{ 
    [Header("Special Setting")]
    public float sailSpeed = 5.0f;
    [SerializeField]protected Tile targetTile;
    protected GameObject boatPrefab;
    [Header("route setting")]
    public float curveSpeed = 0.25f;
    [SerializeField] protected List<BezierRoute> routes;
    private Unit player;
    private GameObject boat;
    
    public override void TileEventActive(Unit player)
    {
        this.player = player;
        Debug.Log("Lucky!! move to " + targetTile.index + " tile");
          
        if (routes.Count > 0)
            StartCoroutine("MoveAlongMultiCurve");
        else
            StartCoroutine("MoveToTarget");
        if (boatPrefab == null)
            return;
        boat = Instantiate(boatPrefab, player.transform.position, Quaternion.identity);
        boat.transform.LookAt(targetTile.transform, Vector3.up);
    }
    public void Start()
    { 
        //name = "SeaRoute path";
        suspendedEvent = false;
        allowToPushBack = true;
        availableToPlaceItem = false;
        removeByCannon = false;
        if (routes.Count < 1)
            Debug.unityLogger.LogWarning(name, "Missing route path. Use normal line to guide.");
        if (!targetTile)
            Debug.unityLogger.LogWarning(name, "Missing target tile");
        boatPrefab = GameManager.Instance.boat;
        boat = null;
    }
    IEnumerator MoveToTarget()
    {
        player.TileIndexSetup(targetTile);
        player.transform.LookAt(targetTile.transform, Vector3.up);
        Vector3 nextPos = player.GetUnitPosition(targetTile.transform.position);
        while (player.transform.position != nextPos)
        {
            player.transform.position = Vector3.MoveTowards(player.transform.position, nextPos, sailSpeed * Time.deltaTime);
            if (boat != null) boat.transform.position = player.transform.position;
            yield return null;
        }
        if (boat != null) Destroy(boat);
        boat = null;
        yield return new WaitForSeconds(1f);
        player.myEvent = null;
        //Debug.Log("Finish sea route move");
    }
    IEnumerator MoveAlongMultiCurve()
    {
        int current = 0;
        float progress = 0;
        Vector3 playerPos = Vector3.zero;
        player.TileIndexSetup(targetTile);
        while (current < routes.Count)
        {
            progress += Time.deltaTime * curveSpeed;
            playerPos = routes[current].getCurvePosition(progress);
            player.transform.position = player.GetUnitPosition(playerPos);
            if (boat != null) boat.transform.position = player.transform.position;
            if (progress >= 1)
            {
               // Debug.Log("start new route");
                progress = 0;
                current++;
            }
            yield return null;
        }
        if (boat != null) Destroy(boat);
        boat = null;
        yield return new WaitForSeconds(1f);
        player.myEvent = null;
        //Debug.Log("End process.Complete multi curve.");
    }
    private void OnDrawGizmos()
    {
        if (routes.Count>0||targetTile==null)
            return;
        Gizmos.color = Color.green;
        Gizmos.DrawLine(transform.position, targetTile.transform.position);
        Vector3 targetPos = targetTile.transform.position;
        Vector3 newPos = new Vector3(targetPos.x, targetPos.y+0.25f, targetPos.z);
        Gizmos.DrawCube(newPos,new Vector3(0.25f,0.25f,0.25f));
    }
}
