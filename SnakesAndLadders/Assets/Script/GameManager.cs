﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DG.Tweening;

public enum GameState
{
    MainTurn,StartTurn, PlayerTurn, EndTurn, EventTurn, MoveTurn
}
[RequireComponent(typeof(AudioSource))]
public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    public GameState m_gameStage { get { return gameState; } set { gameState = value; } }
    public int diceValue { get { return totalDiceValue; } }
    public int mainTurn { get { return turn; } }
    public Unit player { get { return currentPlayer.unitPlayer; } }
  
    [Header("Game Turn")]
    [SerializeField]
    protected GameState gameState = GameState.MainTurn;
    [SerializeField]
    [Range(1, 4)] protected int totalUnits = 2;
    public static int totalUnitDataPass = 0;
    [SerializeField] protected int turn = 0;
    [SerializeField] protected int playerTurn = 1;
    public static bool rollDiceEnable = true;
    [SerializeField] protected bool Breakgameover = false;
    protected Player currentPlayer;
    protected Player latePlayer;
    [Header("Camera")]
    public GameObject gameCam;
    public GameObject winCam;
    [Header("Cursor")]
    public GameObject cursorPrefab;
    public LayerMask selectMask;
    [Header("Player unit")]
    public List<Player> unitsPrefab;
    [Header("Dices and Roll")]
    public Transform diceSpawner;
    public GameObject dicePlateSpawner;
    [SerializeField]protected float diceAnimationDelay = 1;
    [SerializeField]protected float diceUIDelay = 1;
    private GameObject diceAnimation;
    private GameObject dicePrefab;
    private int totalDiceValue = 0;
    private bool roll = false;
    [Header("Tiles")]
    public GameObject tilesStack;
    private List<Tile> tiles =new List<Tile>();
    public Tile startTile { get { return tiles[0]; } }
    public Tile finishTile { get { return tiles[tiles.Count - 1]; } }
    [Header("WinningScene")]
    public Transform[] playersRankPos;
    [Header("Inventory Slot UI")]
    public List<Image> itemSlotUI;
    [Header("Canvas & UI")]
    public GameObject gameplayPanelUI;
    public GameObject winningPanelUI;
    public GameObject mainTurnCutSceneUI;
    public Text mainTurnNumber;
    public Text mainTurnText;
    public Button diceButton;
    public Button mapButton;
    bool openFreeMap;
    [SerializeField]protected float cutsceneDelay = 2.2f;
    public GameObject diceNumberCanvasUI;
    public NumberController[] diceNumberText;
    public Text helpText;
    public GameObject[] playerIconUI;
    public Text turnDebugText;
    public Text stateDebugText;
    public Text eventDebugText;
    [Header("Sound")]
    public static SoundManager soundManager;
    private AudioSource audioSource;
    public AudioClip GameplayBGM;
    public AudioClip WinningBGM;
    [Header("SFX sound")]
    public AudioClip VictorySound;
    public AudioClip newMainTurnSound;
    public AudioClip newPlayerSound;
    public AudioClip errorSound;
    public AudioClip diceRollSound;
    public AudioClip tileBreakSound;
    public AudioClip placeItemSound;
    [Header("Dice Value RandomWeight")]
    public List<MoveValueWeight> numberWeight;
    private RandomWeightScript<int> numberDrops; 
    [Header("Dice Animation Prefab")]
    public GameObject[] dice0;
    public GameObject[] dice1;
    public GameObject[] dice2;
    public GameObject[] dice3;
    public GameObject[] dice4;
    public GameObject[] dice5;
    public GameObject[] dice6;
    public GameObject[] dice7;
    public GameObject[] dice8;
    public GameObject[] dice9;
    public GameObject dice10;
    [Header("ModelPrefab")]
    public GameObject cannon;
    public GameObject boat;
    private bool isGameOver;
    private IEnumerator coroutine;

    [System.Serializable]
    public class Player 
    {
        public Unit unitPlayer;
        public int rank = 0;
        public bool finishing = false;
    }
    [System.Serializable]
    public class MoveValueWeight
    {
        public int value;
        public double weight;

    }
    // event
    public void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(gameObject);

        Debug.Assert(tilesStack, "Missing Tile Stack ");
        //Debug.Log("mainmenu sent totalplayer data " + totalUnitDataPass);
        if (totalUnitDataPass != 0)
            totalUnits = totalUnitDataPass;
      
        if (tilesStack == null)
            return;
        foreach(Transform child in tilesStack.transform)
        {
            Tile t = child.GetComponent<Tile>();
            tiles.Add(t);
        }
        if(totalUnitDataPass==0)
            DOTween.Init();
      
       }
    void Start()
    {
        if (soundManager == null)
        {
            soundManager = FindObjectOfType<SoundManager>();
           // Debug.Log(soundManager.name);
        }
        audioSource = this.GetComponent<AudioSource>();
        audioSource.outputAudioMixerGroup = SoundManager.Instance.Mixer.FindMatchingGroups("SFX")[1];
        //tile index
        foreach (Tile t in tiles)
        {
            t.index = tiles.IndexOf(t);
            string word = t.index.ToString();
            if (t.myEvent)
            {
               word =word+" (Evnet) ";
            }
            t.name = t.name+" " + word;
        }
        //dice UI and prefab
        Debug.Assert(diceNumberCanvasUI, "missing diceShowNumber UI");
        diceNumberCanvasUI.SetActive(false);
      
        Debug.Assert(diceButton, "missing dice button");
        if (diceButton) diceButton.onClick.AddListener(delegate { RollTheDice(); });
        if (dicePlateSpawner) dicePlateSpawner.SetActive(false);
        //winner canvas UI
        openFreeMap = false;
        mapButton.onClick.AddListener(() =>CameraModeChange());
        if(winningPanelUI)  winningPanelUI.SetActive(false);
        if(mainTurnCutSceneUI) mainTurnCutSceneUI.SetActive(false);
        if (helpText == null)
            Debug.LogWarning("missing help text");
        if(helpText) helpText.gameObject.SetActive(false);
        //sound
        Debug.Assert(GameplayBGM, "missing gameplay BGM sound");
        Debug.Assert(WinningBGM, "missing winning BGM sound");
        Debug.Assert(VictorySound, "missing victory Sound");
        Debug.Assert(newPlayerSound, "missing new player Sound");
        Debug.Assert(newMainTurnSound, "missing new main turn Sound");
        //cursor for event
        Debug.Assert(cursorPrefab, "Missing Cursor Prefab");
        //player unit setup            
        if (unitsPrefab.Count <= 1)
            Debug.LogError("Missing unit in GameManager (need 2 players to play this game)");
        else
            foreach (Player p in unitsPrefab)
            {
                p.finishing = false;
                p.rank = 0;
            }

        //GameManager setup
        if (GameplayBGM)
        {
            //Debug.unityLogger.Log("soundmanager instance");
            //soundManager.PlayBGMWithFadeIn(GameplayBGM);
            SoundManager.Instance.PlayBGMWithFadeIn(GameplayBGM);
        }
        isGameOver = false;
        turn = 1;
        playerTurn = 1;
        currentPlayer = unitsPrefab[0];
       
        UpdateUI();
        AddRandomWeightData();
    }
    void Update()
    {
        if (unitsPrefab == null)
            return;
        //test control
        if (Input.GetKeyDown(KeyCode.Space))
            RollTheDice();

        if (Input.GetKeyDown(KeyCode.Z))
            UpdateRankPlayer();

        if (Input.GetKeyDown("x"))
        {
            Debug.unityLogger.Log(name, "<color=purple>randomWeight update</color>");
            AddRandomWeightData();
        }

        if (Input.GetKeyDown(KeyCode.C))
        {
            Debug.unityLogger.Log(name, "<color=purple>force end turn complete</color>");
            gameState = GameState.EndTurn;
        }
         
       
        if (isGameOver)
            return;

        //TextLogUIControl();
        //game loop control
        if (playerTurn > totalUnits) //update main turn
        {
            UpdateRankPlayer();
            playerTurn = 1;
            turn++;
            gameState = GameState.MainTurn;
        }
        currentPlayer = unitsPrefab[playerTurn - 1]; //update current player
        Unit current = currentPlayer.unitPlayer;
        DebugUI();
        if (gameState==GameState.MainTurn)
        {
            //Debug.unityLogger.Log(name, "<color=purple>Start Turn </color> " + turn);
           // CameraManager.Instance.StartStageCamera();
            ResetInventoryUI();
            MainTurnCutScene();
            CameraManager.Instance.StartCamera();
            //m_gameStage = GameState.StartTurn;
            //show main turn ui delay time ... 
        }
        else if (gameState == GameState.StartTurn)
        {
            Debug.Log(current.name + "[<B><color=yellow> Start turn.</color></b>]");
            if (currentPlayer.finishing)
            {
                Debug.Log(current.name + " already finish the game");
                gameState = GameState.EndTurn;
                return;
            }

            if (current.jailed)
            {
                Debug.Log(current.name + " is jailed.");
                gameState = GameState.EndTurn;
                return;
            }
            UpdateInventoryUI();
            UpdateUI();
            audioSource.PlayOneShot(newPlayerSound);
            gameState = GameState.PlayerTurn;
            CameraManager.Instance.NormalStageCamera();
        }
        else if (gameState==GameState.PlayerTurn)
        {
            current.BeginTurn();
            //CameraManager.Instance.StartStageCamera();
        }
        else if (gameState == GameState.MoveTurn)
        {
            return;
        }
        else if (gameState == GameState.EventTurn)
        {
            if(current.myEvent==null)
                CheckPlayersAction();
            UpdateInventoryUI();
        }
        else if (gameState == GameState.EndTurn)
        {
            Debug.Log(current.name + " [<B><color=green> End turn.</color></b>]");
            mapButton.gameObject.SetActive(true);//****
            latePlayer = currentPlayer;
            ResetInventoryUI();

            //new turn count
            IsPlayerReachToFinish(currentPlayer);
            current.FinishTurn();
            playerTurn++;
            roll = false;
            gameState = GameState.StartTurn;

        }
        //game loop control**
    }
       
    //dice 
    public void RollTheDice() 
    {
        if (roll || player.playable==false)
            return;

        mapButton.gameObject.SetActive(false);

        if (!rollDiceEnable)
        {
            totalDiceValue = numberDrops.GetRandom();
            roll = true;
            Debug.Log("< " + totalDiceValue + " >");
            if (coroutine == null)
            {
                coroutine = RollDiceAnimationDisable();
                StartCoroutine(coroutine);
                roll = true;
            }
            //WriteDialog("Complete. Your dice value is " + totalDiceValue);
            //player.PlayerRollDice();
            return;
        }
     
        totalDiceValue = numberDrops.GetRandom();
        Debug.Log("< " + totalDiceValue + " >");
        if (coroutine == null)
        {
            coroutine = RollDiceAnimationEnable();
            StartCoroutine(coroutine);
            roll = true;
        }
      
    }
    IEnumerator RollDiceAnimationDisable()
    {
        audioSource.PlayOneShot(diceRollSound);
        diceNumberCanvasUI.SetActive(true);
        NumberUIUpdate(totalDiceValue);
        yield return new WaitForSeconds(diceUIDelay);
        diceNumberCanvasUI.SetActive(false);
        player.PlayerRollDice();
        coroutine = null;
    }
    IEnumerator RollDiceAnimationEnable()
    {
        audioSource.PlayOneShot(diceRollSound);
        if (dicePlateSpawner)
            dicePlateSpawner.SetActive(true);
        switch (totalDiceValue) 
        {
            case 0:diceAnimation = dice0[Random.Range(0,dice0.Length)]; break;
            case 1:diceAnimation = dice1[Random.Range(0, dice1.Length) ];break;
            case 2:diceAnimation = dice2[Random.Range(0, dice2.Length) ];break;
            case 3:diceAnimation = dice3[Random.Range(0, dice3.Length) ];break;
            case 4:diceAnimation = dice4[Random.Range(0, dice4.Length) ];break;
            case 5:diceAnimation = dice5[Random.Range(0, dice5.Length) ];break;
            case 6:diceAnimation = dice6[Random.Range(0, dice6.Length) ];break;
            case 7:diceAnimation = dice7[Random.Range(0, dice7.Length) ];break;
            case 8:diceAnimation = dice8[Random.Range(0, dice8.Length) ];break;
            case 9:diceAnimation = dice9[Random.Range(0, dice9.Length) ];break;
            case 10:diceAnimation = dice10;break;
        }
        dicePrefab = Instantiate(diceAnimation, diceSpawner.position, Quaternion.identity);
        dicePrefab.transform.SetParent(diceSpawner);
        yield return new WaitForSeconds(diceAnimationDelay);
        diceNumberCanvasUI.SetActive(true);
        NumberUIUpdate(totalDiceValue);
        yield return new WaitForSeconds(diceUIDelay);
        diceNumberCanvasUI.SetActive(false);
        if (dicePlateSpawner)
        dicePlateSpawner.SetActive(false);
        if (dicePrefab)
            Destroy(dicePrefab);
        player.PlayerRollDice();
        coroutine = null;
     }
    //tile 
    public Tile FindNearestCheckPointTile(int tileindex)
    {
        for(int i = tileindex - 1; i > 0; i--)
        {
            if (tiles[i].Ischeckpoint)
                return tiles[i];
        }
        return startTile;
    }
    public Tile GetTile(int index)
    {
        return tiles[index];
    }
    //GameManager System
    private void CheckPlayersAction()  
    {
        //Debug.Log("Ending event start");
        if (coroutine == null)
        {
            coroutine = EndTurnPlayerCheck();
            StartCoroutine(coroutine);
        }
    }
    IEnumerator EndTurnPlayerCheck()
    {
        //Debug.unityLogger.LogWarning(name,"<b><color=yellow>Start checking player </color></b>");
        Debug.Log("<b><color=yellow>Start checking player </color></b>");
        bool stop = false;
        while (stop == false)
        {
            //Debug.Log("<color=pink>Tick</color>");
            yield return new WaitForSeconds(2f);
            //stop = player.isSleeping && player.myEvent == null && player.trading == false && player.playAnimation == false;
            for (int i = 0; i < totalUnits; i++)
            {
                Player p = unitsPrefab[i];
                stop = p.unitPlayer.isSleeping && p.unitPlayer.myEvent == null && p.unitPlayer.trading == false 
                    && p.unitPlayer.playAnimation == false&&currentPlayer.unitPlayer.traped==false;
                if (!stop)
                    break;
                else
                    continue;
            }
        }
        //Debug.unityLogger.LogWarning(name, "<b><color=green>All player completely stop moving.</color></b>");
        Debug.Log("<b><color=green>All player completely stop moving.</color></b>");
        coroutine = null;
        gameState = GameState.EndTurn;
    }
    private void UpdateRankPlayer()
    {
        Debug.unityLogger.Log(name, "<color=purple>rank update</color>");
        //Debug.Log("rank update");
        List<Player> bubble = new List<Player>();
        int rank = 1;
        //add data
        for (int i = 0; i < totalUnits; i++)
            bubble.Add(unitsPrefab[i]);

        //last to first
        for(int i = 0; i < bubble.Count; i++) //more <= >lest  
        {
            for(int j = 0; j < bubble.Count; j++) 
            {
                if (bubble[i].unitPlayer.currentTile > bubble[j].unitPlayer.currentTile)
                {
                    Player temp = bubble[i];
                    bubble[i] = bubble[j];
                    bubble[j] = temp;
                }
            }
        }
       
        //add rank
        foreach (Player p in bubble)
        {
            p.rank = rank;
            rank++;
        }

    }
    private void IsPlayerReachToFinish(Player player)// current player finish the line?  //....need to check >>
    {
        if (player.unitPlayer.currentTile != finishTile.index)
            return;
        Debug.Log(" <b><color=green>" + player.unitPlayer.name + " is finishing the line</color></b>");
        player.finishing = true;
        UpdateRankPlayer();
        if (!Breakgameover) //debug and test
            GameOver();
    }
    private void GameOver()
    {
        isGameOver = true;
        audioSource.PlayOneShot(VictorySound);
        foreach (Player p in unitsPrefab)
        {
            p.unitPlayer.FinishTurn();
            if (p.rank == 1)
                p.unitPlayer.PlayWinSPAnimation();
            else
                p.unitPlayer.PlaySadSPAnimation();
        }
        Debug.Log("<color=red>Game Over</color>");
        soundManager.VolumeFadeOut();
        Invoke("StartWinningScene", 1.75f);
    }
    private void StartWinningScene()
    {
        if (WinningBGM)
            soundManager.PlayBGMWithFadeIn(WinningBGM);
        gameplayPanelUI.SetActive(false);
        winningPanelUI.SetActive(true);
        List<Player> bubble = new List<Player>();
        for (int i = 0; i < totalUnits; i++)
            bubble.Add(unitsPrefab[i]);

        for (int i = 0; i < bubble.Count; i++) //more <= >lest  
        {
            for (int j = 0; j < bubble.Count; j++)
            {
                if (bubble[i].rank < bubble[j].rank)
                {
                    Player temp = bubble[i];
                    bubble[i] = bubble[j];
                    bubble[j] = temp;
                }
            }
        }
        for(int i = 0; i < totalUnits; i++)
        {
            Transform player = bubble[i].unitPlayer.transform;
            player.position= playersRankPos[i].position;
            player.eulerAngles = playersRankPos[i].eulerAngles;
        }
        gameCam.SetActive(false);
        winCam.SetActive(true);
    }
    //UI
    public void WriteHelpText(string word,bool showword)//open word
    {
        helpText.gameObject.SetActive(showword);
        //helpText.color = Color.white;
        helpText.text = "Help: "+ word;
    }
    public void WriteHelpText(string word) //normal word
    {
       // helpText.color = Color.white;
        helpText.text = "Help: " + word;
    }
    public void WriteHelpErrorText(string word)
    {
        //helpText.color = Color.red;
        helpText.text = word;
        helpText.rectTransform.DOShakePosition(1,Vector3.one, 20, 90, false, false).SetAutoKill(true);

        if (audioSource.isPlaying) audioSource.Stop();
        audioSource.volume = 0.5f;
        audioSource.PlayOneShot(errorSound);
    }
    void CameraModeChange()
    {
        if (isGameOver)
            return;
      
        openFreeMap = !openFreeMap;
        if (openFreeMap)
        {
            diceButton.gameObject.SetActive(false);
            CameraManager.Instance.SwapCameraMode();
            WriteHelpText("mouse to control direction | scroll to zoom in/out", true);
        }
        else
        {
            diceButton.gameObject.SetActive(true);
            CameraManager.Instance.SwapCameraMode();
            WriteHelpText("FREEMODE CAMERA: mouse to control direction | scroll to zoom in/out", false);
        }
    }
    public void PlayTileBreakSound()
    {
        audioSource.volume = 0.15f;
        audioSource.PlayOneShot(tileBreakSound);
    }
    public void PlayPlaceitemSound()
    {
        audioSource.volume = 0.5f;
        audioSource.PlayOneShot(placeItemSound);
    }
    
    private void NumberUIUpdate(int value)
    {
        foreach (NumberController num in diceNumberText)
            num.UpdateNumber(value);
    }
    private void DebugUI()
    {
        if (mainTurnText)
            mainTurnText.text = "- " + "Turn " + turn.ToString() + " -";
        //UI control 
        stateDebugText.text = gameState.ToString();

        if (currentPlayer == null)
        {
            turnDebugText.text = turn.ToString() + " Turn > Player " + playerTurn;
            eventDebugText.text = "Event :";
        }
        else
        {
            turnDebugText.text = turn.ToString() + " Turn > " + currentPlayer.unitPlayer.name;
            eventDebugText.text = "Event : " + currentPlayer.unitPlayer.myEvent;
        }

    }
    private void UpdateUI()
    {
        foreach (GameObject icon in playerIconUI)
        {
            icon.SetActive(false);
        }
        playerIconUI[playerTurn - 1].SetActive(true);
    }
    private void MainTurnCutScene()
    {
        if(coroutine==null)
        {
            coroutine = MainTurnCutSceneActive();
            StartCoroutine(coroutine);
        }
    }
    IEnumerator MainTurnCutSceneActive()
    {
        Debug.unityLogger.Log(name, "<color=purple>Start Turn </color> " + turn);
        mainTurnCutSceneUI.SetActive(true);
        mainTurnNumber.text = turn.ToString("00");
        audioSource.PlayOneShot(newMainTurnSound);
        yield return new WaitForSeconds(cutsceneDelay);
        mainTurnCutSceneUI.SetActive(false);
        gameState = GameState.StartTurn;
        coroutine = null;
    }
    //inventoryUI
    public void UpdateInventoryUI()
    {
       for (int i = 0; i <player.inventory.Count; i++)
        {
            Color c = itemSlotUI[i].color;
            c.a = 1;
            itemSlotUI[i].color = c;

            itemSlotUI[i].sprite = player.inventory[i].icon;
        }
    } 
    public void ResetInventoryUI()
    {
        foreach(Image icon in itemSlotUI)
        {
            Color c = icon.color;
            c.a = 0;
            icon.color = c;
        }
    }

    //random weight
    private void AddRandomWeightData()
    {
        if (numberWeight.Count < 1)
            return;

        numberDrops = new RandomWeightScript<int>();
        foreach(MoveValueWeight num in numberWeight)
        {
            numberDrops.AddEntry(num.value, num.weight);
        }
    }
   
}
