﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using DG.Tweening;

public class MainMenuSceneManager : MonoBehaviour
{
    public string gameplayScene = "NewMap";
    [Header("menuUI")]
    public GameObject mainMenuPanel;
    public GameObject optionPanel;
    public GameObject selectPanel;
    public GameObject transitionPanel;
    public float duration = 500f;
    [Header("ButtonUI")]
    [Header("MainMenu")]
    public Button startButton;
    public Button optionButton;
    public Button exitButton;
    [Header("Option")]
    public Button optionToMenuButton;
    [Header("SelectGame")]
    public Button selectToMenuButton;
    public Button twoPlayerButton;
    public Button threePlayerButton;
    public Button fourPlayerButton;
    [Header("Sound Setting")]
    AudioSource audioSource;
    public AudioClip OnclickButtonSound;
    public AudioClip MainMenuBGMSound;
    private void Start()
    {
        DOTween.Init();
        audioSource = this.GetComponent<AudioSource>();
        audioSource.outputAudioMixerGroup = SoundManager.Instance.Mixer.FindMatchingGroups("UI")[0];
        if (MainMenuBGMSound)
        {
            SoundManager.Instance.PlayBGMWithFadeIn(MainMenuBGMSound);
        }
        if (mainMenuPanel.activeSelf==false) mainMenuPanel.SetActive(true);
        if(optionPanel.activeSelf==true)    optionPanel.SetActive(false);
        if(selectPanel.activeSelf == true) selectPanel.SetActive(false);
        //main menu
        startButton.onClick.AddListener(delegate { GoToSelectGame(); OnClickSound(); });
        optionButton.onClick.AddListener(delegate { GoToOption(); OnClickSound(); });
        exitButton.onClick.AddListener(delegate { ExitGame(); });
        //option
        optionToMenuButton.onClick.AddListener(delegate { BackToMainMenu(); OnClickSound(); });
        //select game
        selectToMenuButton.onClick.AddListener(delegate { BackToMainMenu(); OnClickSound(); });
        twoPlayerButton.onClick.AddListener(delegate { StartGameScene(2); OnClickSound(); });
        threePlayerButton.onClick.AddListener(delegate { StartGameScene(3); OnClickSound(); });
        fourPlayerButton.onClick.AddListener(delegate { StartGameScene(4); OnClickSound(); });
    }
  
    void StartGameScene(int totalPlayer)
    {
        GameManager.totalUnitDataPass = totalPlayer;
        GameManager.soundManager = FindObjectOfType<SoundManager>();
        SoundManager.Instance.VolumeFadeOut(); //2 sec
        FadeTransition(duration);
        Invoke("LoadScene",2);
    } 
    void LoadScene()
    {
        SceneManager.LoadScene(gameplayScene);
    }
    void ExitGame()
    {
        Debug.Log("exit");
        Application.Quit();
    }
    void BackToMainMenu()
    {
        //OnClickSound();
        mainMenuPanel.SetActive(true);
        optionPanel.SetActive(false);
        selectPanel.SetActive(false);
    }
    void GoToSelectGame()
    {
        //OnClickSound();
        mainMenuPanel.SetActive(false);
        selectPanel.SetActive(true);
    }
    void GoToOption()
    {
        //OnClickSound();
        mainMenuPanel.SetActive(false);
        optionPanel.SetActive(true);
    }
    void OnClickSound()
    {
        if (audioSource.isPlaying) audioSource.Stop();
        audioSource.PlayOneShot(OnclickButtonSound);
    }
    void OnClickSound(AudioClip sound)
    {
        if (audioSource.isPlaying)
            audioSource.Stop();
        audioSource.PlayOneShot(sound);
    }
    void FadeTransition(float duration)
    {
        Image panel = transitionPanel.GetComponent<Image>();
        panel.DOFade(255,duration).SetAutoKill(true);
    }

}
