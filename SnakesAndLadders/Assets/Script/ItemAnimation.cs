﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemAnimation : MonoBehaviour
{
    public Animator MyAnimator { get { return animator; } }
    Animator animator;
    private void Start()
    {
        animator = this.GetComponent<Animator>();
    }
    public void PlayAnimation()
    {
        animator.SetTrigger("PlayerEnter");
    }
   
}
