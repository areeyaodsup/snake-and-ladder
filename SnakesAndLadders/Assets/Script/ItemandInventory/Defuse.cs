﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Defuse : Item
{
    public override void useItem(Unit player)
    {
        Debug.Log("Dynamite defused");
    }
    public void Start()
    {
        instantlyUsed = false;
    }

}
