﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public abstract class Item :MonoBehaviour
{
    public int id;
    public string description;
    public GameObject model;
    public GameObject AnimationModel;
    public Sprite icon;
    public bool instantItem { get { return instantlyUsed; } }
    public bool beActivate { get { return activate; } }
    public int timeToReModel { get { return timeToResetModel; } }
    protected bool instantlyUsed;
    protected bool activate;
    protected int timeToResetModel =0;
    public abstract void useItem(Unit player);
   
}
