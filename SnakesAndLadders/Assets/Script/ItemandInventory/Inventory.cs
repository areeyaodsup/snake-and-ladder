﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    public List<Slot> slots;
    [SerializeField] private Unit player;
    public void Start()
    {
       // GameManager.Instance.OnEndingTurn += ResetInventory;
       // GameManager.Instance.OnStartingTurn += UpdatePlayerSlot;
       
    }

    public void Update()
    {
        if (Input.GetKeyDown("z"))
        {
            ResetInventory();
            UpdatePlayerSlot();
        }
        if (Input.GetKeyDown("x"))
            ResetInventory();
    }

    public void UpdatePlayerSlot()
    {
        player = GameManager.Instance.player;
      //for(int j = 0; j < player.inventory.Count; j++)
      //  {
      //      Debug.Log(j);
      //      for(int k = 0; k < slots.Count; k++)
      //      {
      //          if (slots[k].isFull)
      //              continue;
      //          slots[k].AddItem(player.inventory[j]);
      //          Debug.Log(j + "" + k);
      //          break;

      //      }
      //  }
        foreach (Item list in player.inventory)
        {
            foreach (Slot slot in slots)
            {
                if (slot.isFull)
                    continue;
                slot.AddItem(list);
                break;
            }
        }
    }

    public void ResetInventory()
    {
        //Debug.Log("reset");
        foreach (Slot slot in slots)
            slot.ResetSlot();
    }
}
