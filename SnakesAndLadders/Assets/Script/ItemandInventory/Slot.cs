﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Slot : MonoBehaviour
{
    public bool isFull{ get { return item!=null?true:false; } }
    public Item myitem { get { return item; } }
    [SerializeField] protected Item item;
    [SerializeField] protected Image icon;
   
    public void AddItem(Item item)
    {
        this.item = item;
        if (item.icon)
        {
            icon.sprite = item.icon;
            Color color = icon.color;
            color.a = 1.0f;
            icon.color = color;
        }
    
    }
  
    public void ResetSlot()
    {
        item = null;
        Color color = icon.color;
        color.a = 0f;
        icon.color = color;
    }
}
