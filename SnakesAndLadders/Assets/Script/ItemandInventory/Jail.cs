﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jail : Item
{
    [Header("Jail setup")]
    public int turnToReset = 1;
    private int counterItemID =0; //none
    private CounterItemCheck counterCheck;
    public override void useItem(Unit player)
    {
        activate = player.UseItemCounter(counterItemID, id) == false;
        if (activate)
        {
            player.JailPlayer(turnToReset);
            timeToResetModel = turnToReset + 1;
        }
        else
        {
            timeToResetModel = -1;
        }
    }
    public void Start()
    {
        instantlyUsed = true;
        activate = false;
        counterCheck = FindObjectOfType<CounterItemCheck>();
        counterItemID = counterCheck.checkCounterItem(id);
    }

}
