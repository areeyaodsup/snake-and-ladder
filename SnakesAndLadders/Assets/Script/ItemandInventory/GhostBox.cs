﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostBox : Item
{
    //[Header("GhostBox setup")]
    public int moveValue = 2;
    private int counterItemID = 0; //none
    private float actionDelay = 3f;
    private CounterItemCheck counterCheck;
    public override void useItem(Unit player)
    {
        activate = player.UseItemCounter(counterItemID, id) == false;
        if (activate)
        {
            StartCoroutine(WaitForAnimation(player));
        }
        else
        {
            timeToResetModel = -1;
        }
    }
    public void Start()
    {
        instantlyUsed = true;
        activate = false;
        counterCheck = FindObjectOfType<CounterItemCheck>();
        counterItemID = counterCheck.checkCounterItem(id);
    }

    private IEnumerator WaitForAnimation(Unit player)
    {
        yield return new WaitForSeconds(actionDelay);
        player.MoveBack(moveValue);
    }
}
