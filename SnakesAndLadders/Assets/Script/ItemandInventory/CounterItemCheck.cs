﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CounterItemCheck : MonoBehaviour
{
   public int checkCounterItem(int id)
    {
        int counterItemId = 0; //none
        switch (id)
        {
            case 1: counterItemId = 2; break; //dynamite <defuse
            case 3: counterItemId = 4; break; //jail<<lockpit
            //default: Debug.Log("NO matching counter item to this item"); break;
        }

        return counterItemId;
    }
}
