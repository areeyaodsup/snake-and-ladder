﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockPick : Item
{
    public override void useItem(Unit player)
    {
        Debug.Log("Jail unlocked!!");
    }

    void Start()
    {
        instantlyUsed = false;
        activate = false;
    }

}
