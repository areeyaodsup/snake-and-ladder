﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class TradeItemSystem : MonoBehaviour
{
    [SerializeField]protected Unit player;
    [SerializeField]protected Item offerItem;
    [SerializeField]protected Item tradeItem;
    protected int indexTrade;
    [Header("Button")]
    public Button exitButton;
    public Button acceptButton;
    public List<Button> slotShowcase;
    [Header("Icon")]
    public Image offerIcon;
    public Image tradeIcon;
    public Image[] slotIcon;
    public Image[] cursorIcon;
  
    private string ID = "Trade System";
    private RectTransform rectPos;
    [SerializeField]private Vector3 starterPos; 
    
    private void Start()
    {
        //available = false;

        exitButton.onClick.AddListener(delegate { CancelTrade(); });
        acceptButton.onClick.AddListener(delegate { ConfirmTrade(); });
        foreach (Button b in slotShowcase)
               b.onClick.AddListener(delegate { SelectItem(b); });

        rectPos = gameObject.GetComponent<RectTransform>();
        starterPos = rectPos.anchoredPosition;
        ResetWindows();
    }
    
    public void StartTradeItem(Item item,Unit player)
    {
        Debug.unityLogger.Log(ID, "<color=yellow>Start trading</color>");
        rectPos.anchoredPosition = Vector3.zero;
        //available = true;
        this.player = player;
        this.player.trading = true;
        offerItem = item;

        //show icon
        offerIcon.sprite = offerItem.icon;
        for(int i = 0; i < player.inventory.Count; i++)
        {
            slotIcon[i].sprite = player.inventory[i].icon;
        }
    }
    public void CancelTrade()
    {
        Debug.unityLogger.Log(ID, "<color=red>Cancel trading </color>");
        player.trading = false;
        rectPos.anchoredPosition = starterPos;
        ResetWindows();
        //available = false;
    }
    public void ConfirmTrade()
    {
        player.inventory[indexTrade] = offerItem;
        player.trading = false;
        Debug.unityLogger.Log(ID, "<color=green>Trading Complete</color>");
        rectPos.anchoredPosition = starterPos;
        ResetWindows();
        //available = false;
    }

    private void SelectItem(Button button)
    {
        indexTrade = slotShowcase.IndexOf(button);
        Debug.unityLogger.Log(ID, "<color=yellow>Select item</color> "+indexTrade);
        tradeItem = player.inventory[indexTrade];

        foreach (Image cursor in cursorIcon)
            cursor.enabled = false;

        cursorIcon[indexTrade].enabled = true;
     
        //show Icon 
        tradeIcon.sprite = tradeItem.icon;
        Color a = tradeIcon.color;
        a.a = 1;
        tradeIcon.color = a;

        if (acceptButton.IsActive() == false)
            acceptButton.gameObject.SetActive(true);
    }

    private void ResetWindows()
    {
        foreach (Image cursor in cursorIcon)
            cursor.enabled = false;

        offerItem = null;
        tradeItem = null;
        offerIcon.sprite = null;
        tradeIcon.sprite = null;
        acceptButton.gameObject.SetActive(false);

        Color a = tradeIcon.color;
        a.a = 0;
        tradeIcon.color = a;
    }
}
