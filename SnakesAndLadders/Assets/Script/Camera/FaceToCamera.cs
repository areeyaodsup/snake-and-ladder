﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FaceToCamera : MonoBehaviour
{
    public Camera mainCam;
    // Update is called once per frame
    private void OnEnable()
    {
        if(mainCam==null)
            mainCam = FindObjectOfType<Camera>();
    }
    void Update()
    {
        //transform.LookAt(mainCam.transform);
        Vector3 camPos = mainCam.transform.position;
        Vector3 relativePos = new Vector3(transform.position.x,camPos.y-transform.position.y,transform.position.z);//mainCam.transform.position - transform.position;
        Quaternion rotation = Quaternion.LookRotation(relativePos, Vector3.up);
        transform.rotation = rotation;
    }
}
