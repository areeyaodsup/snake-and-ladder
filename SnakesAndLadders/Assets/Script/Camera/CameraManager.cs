﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    public static CameraManager Instance;
    public CameraController cam;

    private void Start()
    {
        Instance = this;
    }
    public void Update()
    {
        //if (Input.GetKeyDown("e"))
        //    SwapCameraMode();
    }
    public void SwapCameraMode()
    {
        cam.m_cam = cam.m_cam == CameraMode.FreeLook ? CameraMode.FollowPlayer : CameraMode.FreeLook;
        if (cam.m_cam == CameraMode.FreeLook)
            cam.CameraFreeMode();
        else if (cam.m_cam == CameraMode.FollowPlayer)
            cam.CameraFollowMode();
    }
    public void SelectStageCamera()
    {
        //cam.CameraFreeMode();
        cam.CameraCustomeMode(CameraMode.FreeLook,50,1,true);
        //select 
    }
    public void NormalStageCamera()
    {
        if (cam.playerTarget == GameManager.Instance.player.transform)
            return;
        cam.playerTarget = GameManager.Instance.player.transform; //tranform type
        cam.CameraFollowMode();
    }
    public void StartCamera()
    {
        cam.m_cam = CameraMode.StedyCam;
    }
}
