﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Pirate.mechanic
{
    public class GameCamera : MonoBehaviour
    {
        [Header("Camera")]
        [SerializeField] private Camera myCamera;
        public CameraMode c_mode = CameraMode.FreeLook;
        public Vector3 camOffset;
        public float smoothTime = 0.5f;
        public float duration = 1.0f;

        [Header("Follow Mode")]
        public Transform target;
        public Vector3 followAngle;

        [Header("FreeLook Mode")]
        public Vector3 freeAngle;
        public float panSpeed = 20f;
        public float panBorderThickness = 10f;
        public float xMinPanLimit;
        public float yMinPanLimit;
        public float xMaxPanLimit;
        public float yMaxPanLimit;
        public bool fixedPan = true;

        [Header("Zoom Control")]
        public float distance = 10f;
        public float zoomRate = 40f;
        public float minDistance = 5f;
        public float maxDistance = 50f;

        Vector3 velocity;
        [SerializeField] private float desiredDistance;
        private Vector3 diff;
        private Vector3 centerPos;
        private IEnumerator coroutine;

        Vector3 pos = Vector3.zero;
      
        private void Start()
        {
            myCamera = Camera.main;
            desiredDistance = distance;

            if (target == null)
                c_mode = CameraMode.FreeLook;
            asas();

        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Q)&&target!=null)
            {
                c_mode = (c_mode == CameraMode.FollowPlayer) ? CameraMode.FreeLook : CameraMode.FollowPlayer;
                UpdateCameraMode();
            }
        }

        void LateUpdate()
        {
           
            if (c_mode == CameraMode.FreeLook)
            {
                if (Input.GetKey("w"))//|| Input.mousePosition.y >= Screen.height - panBorderThickness)
                {
                   centerPos.z += panSpeed * Time.deltaTime;
                }
                if (Input.GetKey("s"))//|| Input.mousePosition.y <= panBorderThickness)
                {
                   centerPos.z -= panSpeed * Time.deltaTime;
                }
                if (Input.GetKey("a"))//|| Input.mousePosition.x <= panBorderThickness)
                {
                   centerPos.x -= panSpeed * Time.deltaTime;
                }
                if (Input.GetKey("d"))//|| Input.mousePosition.x >= Screen.width - panBorderThickness)
                {
                   centerPos.x += panSpeed * Time.deltaTime;
                }
            }

            //zoom 
            desiredDistance -= Input.GetAxis("Mouse ScrollWheel") * Time.deltaTime * zoomRate * Mathf.Abs(desiredDistance);
            desiredDistance = Mathf.Clamp(desiredDistance, minDistance, maxDistance);
            Vector3 targetPos = (c_mode == CameraMode.FollowPlayer) ? target.position : centerPos;
            Vector3 zoomPos = targetPos - diff * desiredDistance;

            //*
            pos = targetPos;
            //pos = (normalPos + transform.forward)-diff*desiredDistance;
            if (Input.GetKeyDown(KeyCode.Space))
                Debug.Log(centerPos+" "+transform.forward);
            //**

            //result pos
            Vector3 position = (c_mode == CameraMode.FollowPlayer) ? Vector3.SmoothDamp(transform.position, zoomPos, ref velocity, smoothTime): zoomPos;
         
            //set position
            transform.position = zoomPos;
        }

        public void UpdateCameraMode()
        {
            asas();
            //update data
            //Transform finalPos = transform;
            //finalPos.position = (c_mode == CameraMode.FollowPlayer) ? target.position : transform.position;
            //finalPos.rotation = (c_mode == CameraMode.FollowPlayer) ? Quaternion.Euler(followAngle) : Quaternion.Euler(freeAngle);
            //Vector3 diffData = (c_mode == CameraMode.FollowPlayer) ? (target.position + camOffset) - finalPos.position : finalPos.forward;
            //diffData.Normalize();
            //finalPos.position = (c_mode == CameraMode.FollowPlayer) ? target.position - diff * desiredDistance : finalPos.position; new position
            //Vector3 newCenter = new Vector3(finalPos.position.x, 0, 0) + finalPos.forward;

            ////transform.position = finalPos.position;
            ////transform.rotation = finalPos.rotation;
            ////StartCoroutine(SmoothCameraModeChanged(finalPos.position, finalPos.eulerAngles, 1));

            //diff = diffData;
            //centerPos = newCenter;

            //*
            string bug = (c_mode == CameraMode.FollowPlayer) ? "follow" + ((target.position + camOffset) - transform.position) : "forawrd" + transform.forward;
            //Debug.Log(diff + " " + c_mode);
            Debug.Log(bug);
            //**
        }
        public void asas()
        {
            Vector3 newPos = (c_mode == CameraMode.FollowPlayer) ? target.position : transform.position;
            Vector3 newAngle = (c_mode == CameraMode.FollowPlayer) ? followAngle : freeAngle;

            //StartCoroutine(SmoothCameraModeChanged(newPos, newAngle,duration));


            Transform finalPos = transform;
            finalPos.position = (c_mode == CameraMode.FollowPlayer) ? target.position : transform.position;
            finalPos.rotation = (c_mode == CameraMode.FollowPlayer) ? Quaternion.Euler(followAngle) : Quaternion.Euler(freeAngle);
            Vector3 diffData = (c_mode == CameraMode.FollowPlayer) ? (target.position + camOffset) - finalPos.position : finalPos.forward;
            diffData.Normalize();
            //center target
            float zCenter = (target) ? target.position.z : finalPos.position.z;
            Vector3 newCenter = new Vector3(finalPos.position.x, 0, zCenter) + finalPos.forward;
            //new camera pos 
          
            diff = diffData;
            centerPos = newCenter;

        }
        private IEnumerator SmoothCameraModeChanged(Vector3 toPos, Vector3 toAngle, float duration)
        {
            Vector3 current = transform.position;
            Vector3 currentAngle = transform.eulerAngles;
            Vector3 pos = transform.position;
            Vector3 angle = transform.eulerAngles;
            float startTime = Time.time;
            float t = 0;
            while (Time.time - startTime < duration)
            {
                yield return null;
                t = (Time.time - startTime) / duration;
                //position
                //pos.x = Mathf.SmoothStep(current.x, toPos.x, t);
                //pos.y = Mathf.SmoothStep(current.y, toPos.y, t);
                //pos.z = Mathf.SmoothStep(current.z, toPos.z, t);
                pos = Vector3.Lerp(current, toPos, t);
                //angle
                //angle.x = Mathf.SmoothStep(currentAngle.x, toAngle.x, t);
                //angle.y = Mathf.SmoothStep(currentAngle.y, toAngle.y, t);
                //angle.z = Mathf.SmoothStep(currentAngle.z, toAngle.z, t);
                angle = Vector3.Lerp(currentAngle, toAngle, t);

                transform.position = pos;
                transform.rotation = Quaternion.Euler(angle);
            }
            diff = (c_mode == CameraMode.FollowPlayer) ? (target.position + camOffset) - transform.position : transform.forward;
            diff.Normalize();
            float zCenter = (target) ? target.position.z : target.position.z;
            centerPos = new Vector3(transform.position.x, 0, zCenter) + transform.forward;

            coroutine = null;
            Debug.Log("Mode Changed");
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.green;
            Gizmos.DrawCube(pos, Vector3.one);

        }
    }
}
