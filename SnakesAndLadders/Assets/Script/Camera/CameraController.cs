﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CameraMode
{
    FreeLook, FollowPlayer,StedyCam
}
public class CameraController : MonoBehaviour
{
    [Header("Camera")]
    [SerializeField] private Camera myCamera;
    public CameraMode m_cam = CameraMode.FreeLook;
    public float cameraAngle;
    [Tooltip("เวลาที่ใช้การเปลี่ยนโหมดกล้อง")]
    public float duration = 1.0f;
    public GameObject dicespawner;
    public Vector3 spawneroffset;
    [Header("Follow Mode")]
    public Transform playerTarget;
    public Vector3 offset;
    public float smoothTime = 0.5f;
    public float focusBorderThickness = 10;
    private Vector3 velocity;

    [Header("FreeLook Mode")]
    public float panSpeed = 20f;
    public float panBorderThickness = 10f;
    public Vector2 panMinLimit;
    public Vector2 panMaxLimit;
    //public Vector2 heightLimit;
    public bool panLimit = true;
    [SerializeField]private float heightLimit =20;
    //[SerializeField]private float ZoffsetLimit =-5;
    [Header("Zoom Control")]
    public float scrollSpeed = 2f;
    public float fovMin = 20f;
    public float fovMax = 100f;
    [Header("Debug")]
     [SerializeField] Vector3 centerPoint;
   
    private IEnumerator coroutine;
    private void Awake()
    {
        cameraAngle = transform.rotation.eulerAngles.x;
        myCamera = GetComponent<Camera>();
        m_cam = CameraMode.StedyCam;
    }

    void Update()
    {
        if (m_cam == CameraMode.StedyCam)
            return;

        Vector3 pos = transform.position;
  
        //control by keyboard mouse
        if (Input.GetKey("w")  ||Input.mousePosition.y >= Screen.height - panBorderThickness)
        {
            pos.z += panSpeed * Time.deltaTime;
        }
        if (Input.GetKey("s")  ||Input.mousePosition.y <= panBorderThickness)
        {
            pos.z -= panSpeed * Time.deltaTime;
        }
        if (Input.GetKey("a") || Input.mousePosition.x <= panBorderThickness)
        {
            pos.x -= panSpeed * Time.deltaTime;
        }
        if (Input.GetKey("d") || Input.mousePosition.x >= Screen.width - panBorderThickness)
        {
            pos.x += panSpeed * Time.deltaTime;
        }

        if (m_cam == CameraMode.FreeLook)
        {
            float scroll = Input.GetAxis("Mouse ScrollWheel");
            //pos.y -= scroll * scrollSpeed * 100f * Time.deltaTime;
            Camera.main.fieldOfView -= scroll * scrollSpeed * 100 * Time.deltaTime;
        }

        //follow mode
        if (m_cam == CameraMode.FollowPlayer)
        {
            if (playerTarget != null)
            {
               Vector3 centerPos = playerTarget.position + offset;
               pos = Vector3.SmoothDamp(transform.position, centerPos, ref velocity, smoothTime);
            }
  
        }
        //check border

        //free mode

        //check limit
        if (panLimit)
        {
            pos.x = Mathf.Clamp(pos.x, panMinLimit.x, panMaxLimit.x);
            pos.z = Mathf.Clamp(pos.z, panMinLimit.y, panMaxLimit.y);
            //pos.y = Mathf.Clamp(pos.y, heightLimit.x, heightLimit.y);
            Camera.main.fieldOfView = Mathf.Clamp(Camera.main.fieldOfView, fovMin, fovMax);
        }

        //set transform.position
        transform.position = pos;

    }

    private void LateUpdate()
    {
        CameraDebug();
    }
    public void CameraCustomeMode(CameraMode mode,float toFoV,float duration,bool offsetReset) 
    {
        if (coroutine != null)
        {
            //Debug.Log("not complete to change mode. CameraMode in progress");
            return;
        }
        m_cam = mode;
        Vector3 pos = transform.position;
        coroutine = SmoothCameraModeChange(myCamera.fieldOfView, toFoV, duration, offsetReset);
        StartCoroutine(coroutine);
    }
    public void CameraFollowMode()
    {
        if (coroutine != null)
        {
            //Debug.Log("not complete to change mode. CameraMode in progress");
            return;
        }
        m_cam = CameraMode.FollowPlayer;
        Vector3 pos = transform.position;
        coroutine = SmoothCameraModeChange(myCamera.fieldOfView, fovMin,duration, false);
        StartCoroutine(coroutine);
    }
    public void CameraFreeMode()
    {
        if (coroutine != null)
        {
           // Debug.Log("not complete to change mode. CameraMode in progress");
            return;
        }

        m_cam = CameraMode.FreeLook;
        Vector3 pos = transform.position;
        coroutine = SmoothCameraModeChange(myCamera.fieldOfView, myCamera.fieldOfView, duration, true);
        StartCoroutine(coroutine);
    }
    IEnumerator SmoothCameraModeChange(float fromFov, float toFov, float duration,bool offsetReset)
    {
        Vector3 pos = transform.position;
        float startY = pos.y;
        //float newOffset = transform.position.z+ZoffsetLimit;
        float startTime = Time.time;
        float t = 0;
        while (Time.time - startTime < duration)
        {
            yield return null;
            t = (Time.time - startTime) / duration;
            //fov
            myCamera.fieldOfView = Mathf.SmoothStep(fromFov, toFov, t);

            if (offsetReset)
            {
                pos.y = Mathf.SmoothStep(startY, heightLimit, t);
                //pos.z = Mathf.SmoothStep(transform.position.z, newOffset, t);
                transform.position = pos;
            }

        }
        //Debug.Log("Camera change complete "+duration);
        coroutine = null;
    }

   
    void CameraDebug()
    {
        float height = transform.position.y;
        float angle = Mathf.Tan(cameraAngle);
        float newPos = height * (int)angle;

        centerPoint = new Vector3(transform.position.x, 0, newPos+transform.position.z);
        if (dicespawner)
            dicespawner.transform.position = centerPoint + spawneroffset;
        // Bounds cameraBound = new Bounds(b, Vector3.zero);
        //Debug.Log(cameraBound.center);
                  
    }

    private void OnDrawGizmos()
    {
       Gizmos.DrawLine(transform.position, centerPoint);

    }
}

