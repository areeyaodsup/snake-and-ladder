﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NumberController : MonoBehaviour
{
    public Sprite[] listNumber;
    Image sprite;
    public int UnitOfNumber = 1;

    private void Awake()
    {
        sprite = GetComponent<Image>();
    }

    public void UpdateNumber(int number)
    {
        number = (number / UnitOfNumber) % 10;
        sprite.sprite = listNumber[number];
    }
    
}
